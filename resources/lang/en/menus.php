<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */
   
    'frontend' => [
            'purchase_packages' =>  'Purchase Packages'
    ],
   


    'backend' => [
        'access' => [
            'title' => 'Access Management',

            'roles' => [
                'all'        => 'All Roles',
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',
                'main'       => 'Roles',
            ],

            'permissions' => [
                'all'        => 'All Permissions',
                'create'     => 'Create Permission',
                'edit'       => 'Edit Permission',
                'management' => 'Permission Management',
                'main'       => 'Permissions',
            ],

            'users' => [
                'all'             => 'All Users',
                'change-password' => 'Change Password',
                'create'          => 'Create User',
                'deactivated'     => 'Deactivated Users',
                'deleted'         => 'Deleted Users',
                'edit'            => 'Edit User',
                'main'            => 'Users',
                'view'            => 'View User',
            ],
        ],

        'package' => [
                'create'     => 'Create Package',
                'edit'       => 'Edit Permission',
                'management' => 'Package Management',
                'all'        => 'All Packages',
                'deactivated'=> 'Deactivated Packages',
                'deleted'    => 'Deleted Packages',
        ],
        'advertiser' => [
            'title'         => 'Advertisers',
            'all'           => 'All Advertisers',
            'create'        => 'Create Advertiser',
            'deactivated'   => 'Deactivated Advertisers',
            'deleted'       => 'Deleted Advertisers',
            'edit'          => 'Edit Advertiser',
            'main'          => 'Advertisers',
            'view'          => 'View Advertiser',            
        ],

        'log-viewer' => [
            'main'      => 'Log Viewer',
            'dashboard' => 'Dashboard',
            'logs'      => 'Logs',
        ],

        'sidebar' => [
            'dashboard'      => 'Dashboard',
            'general'        => 'General',
            'system'         => 'System',
            'manage_packages'=> 'Manage Packages',
            'manage_porchd'=> 'Manage Porchd',
        ],
        'other' => [
            'title'          => 'Other Management',
            'deactivated'    => 'Deactivated Departments',
            'all_department' => 'All Departments',
            'create'         => 'Create Department',
            'grade'          => [
                'create'     => 'Create Grade',
            ],
            'designation'          => [
                'create'     => 'Create Designation',
            ],
            'country' => [
                'create'     => 'Create Country',
            ],
            'state' => [
                'create'     => 'Create State',
            ],
            'city' => [
                'create'     => 'Create City',
            ],
           
            'roles' => [
                'all'        => 'All Roles',
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',
                'main'       => 'Roles',
            ],

            'permissions' => [
                'all'        => 'All Permissions',
                'create'     => 'Create Permission',
                'edit'       => 'Edit Permission',
                'management' => 'Permission Management',
                'main'       => 'Permissions',
            ],

            
        ],
    ],

   
     


    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => 'Arabic',
            'zh'    => 'Chinese Simplified',
            'zh-TW' => 'Chinese Traditional',
            'da'    => 'Danish',
            'de'    => 'German',
            'el'    => 'Greek',
            'en'    => 'English',
            'es'    => 'Spanish',
            'fr'    => 'French',
            'id'    => 'Indonesian',
            'it'    => 'Italian',
            'ja'    => 'Japanese',
            'nl'    => 'Dutch',
            'pt_BR' => 'Brazilian Portuguese',
            'ru'    => 'Russian',
            'sv'    => 'Swedish',
            'th'    => 'Thai',
            'tr'    => 'Turkish',
        ],
    ],

    
];
