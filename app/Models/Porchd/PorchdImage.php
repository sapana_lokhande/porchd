<?php

namespace App\Models\Porchd;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Porchd.
 */
class PorchdImage extends Model
{

 
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.porchd_image_table');
    }
}
