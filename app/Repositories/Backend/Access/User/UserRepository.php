<?php

namespace App\Repositories\Backend\Access\User;

use App\Models\Access\User\User;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserSubscription\UserSubscription;
use App\Models\Package\Package;
use App\Models\UserPaymentInfo\UserPaymentInfo;
use App\Models\UserPorchdAccount\UserPorchdAccount;
use App\Events\Backend\Access\User\UserCreated;
use App\Events\Backend\Access\User\UserDeleted;
use App\Events\Backend\Access\User\UserUpdated;
use App\Events\Backend\Access\User\UserRestored;
use App\Events\Backend\Access\User\UserConfirmed;
use App\Events\Backend\Access\User\UserDeactivated;
use App\Events\Backend\Access\User\UserReactivated;
use App\Events\Backend\Access\User\UserUnconfirmed;
use App\Events\Backend\Access\User\UserPasswordChanged;
use App\Notifications\Backend\Access\UserAccountActive;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Events\Backend\Access\User\UserPermanentlyDeleted;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL    = User::class;
    const MODELSUB = Package::class;
    const MODELUPA = UserPorchdAccount::class;
    const MODELUS  = UserSubscription::class;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
        $this->model = new User();
    }

    /**
     * @param        $permissions
     * @param string $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (! is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->query()->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.'.$by, $permissions);
        })->get();
    }

    /**
     * @param        $roles
     * @param string $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (! is_array($roles)) {
            $roles = [$roles];
        }

        return $this->query()->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.'.$by, $roles);
        })->get();
    }

    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->with('roles')
            ->select([
                config('access.users_table').'.id',
                config('access.users_table').'.first_name',
                config('access.users_table').'.last_name',
                config('access.users_table').'.email',
                config('access.users_table').'.status',
                config('access.users_table').'.confirmed',
                config('access.users_table').'.created_at',
                config('access.users_table').'.updated_at',
                config('access.users_table').'.deleted_at',
            ]);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount()
    {
        return $this->query()->where('confirmed', 0)->count();
    }

    /**
     * @param array $input
     */
    public function create($input)
    {

        $data         = $input['data'];
        $roles        = $input['roles'];
        $user_credits = (int) $input['user_credits']['user_credits'];
         
        $user = $this->createUserStub($data);

        DB::transaction(function () use ($user, $data, $roles, $user_credits) {
            if ($user->save()) {
                //User Created, Validate Roles
                if (! count($roles['assignees_roles'])) {
                    throw new GeneralException(trans('exceptions.backend.access.users.role_needed_create'));
                }

                //Attach new roles
                $user->attachRoles($roles['assignees_roles']);

                //Send confirmation email if requested and account approval is off
                if (isset($data['confirmation_email']) && $user->confirmed == 0 && ! config('access.users.requires_approval')) {
                    $user->notify(new UserNeedsConfirmation($user->confirmation_code));
                }

                 //User credits
                $user_subscriptions = $this->createUserCreditsStub($user,$user_credits);
                $user_subscriptions->save();

                 //User credits
                $porchd_account = $this->createUserPorchdAccountStub($user,$user_credits);
                $porchd_account->save();

                event(new UserCreated($user));

               

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
        });
    }

    /**
     * @param Model $user
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $user, array $input)
    {
        $data         = $input['data'];
        $roles        = $input['roles'];
        $user_credits = (int) $input['user_credits']['user_credits'];
        
        $this->checkUserByEmail($data, $user);

        $user->first_name       = $data['first_name'];
        $user->last_name        = $data['last_name'];
         // 27 Oct 2017 Added U sername start
        $user->username         = $data['username'];
        $user->state_assignment = $data['state_assignment'];
         // 27 Oct 2017 Added Username end  
        $user->email = $data['email'];
        $user->status = isset($data['status']) ? 1 : 0;

        DB::transaction(function () use ($user, $data, $roles, $user_credits) {
            if ($user->save()) {
                $this->checkUserRolesCount($roles);
                $this->flushRoles($roles, $user);
                //User credits
                $user_subscriptions = $this->createUserCreditsStub($user,$user_credits);
                $user_subscriptions->save();

                 //User credits
                $porchd_account = $this->createUserPorchdAccountStub($user,$user_credits);
                $porchd_account->save();

                event(new UserUpdated($user));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
        });
    }

    /**
     * @param Model $user
     * @param $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function updatePassword(Model $user, $input)
    {
        $user->password = bcrypt($input['password']);

        if ($user->save()) {
            event(new UserPasswordChanged($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.update_password_error'));
    }

    /**
     * @param Model $user
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Model $user)
    {
        if (access()->id() == $user->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if ($user->id == 1) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_admin'));
        }

        if ($user->delete()) {
            event(new UserDeleted($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

    /**
     * @param Model $user
     *
     * @throws GeneralException
     */
    public function forceDelete(Model $user)
    {
        if (is_null($user->deleted_at)) {
            throw new GeneralException(trans('exceptions.backend.access.users.delete_first'));
        }

        DB::transaction(function () use ($user) {
            if ($user->forceDelete()) {
                event(new UserPermanentlyDeleted($user));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
        });
    }

    /**
     * @param Model $user
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function restore(Model $user)
    {
        if (is_null($user->deleted_at)) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_restore'));
        }

        if ($user->restore()) {
            event(new UserRestored($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

    /**
     * @param Model $user
     * @param $status
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function mark(Model $user, $status)
    {
        if (access()->id() == $user->id && $status == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $user->status = $status;

        switch ($status) {
            case 0:
                event(new UserDeactivated($user));
            break;

            case 1:
                event(new UserReactivated($user));
            break;
        }

        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param Model $user
     *
     * @return bool
     * @throws GeneralException
     */
    public function confirm(Model $user)
    {
        if ($user->confirmed == 1) {
            throw new GeneralException(trans('exceptions.backend.access.users.already_confirmed'));
        }

        $user->confirmed = 1;
        $confirmed = $user->save();

        if ($confirmed) {
            event(new UserConfirmed($user));

            // Let user know their account was approved
            if (config('access.users.requires_approval')) {
                $user->notify(new UserAccountActive());
            }

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.cant_confirm'));
    }

    /**
     * @param Model $user
     *
     * @return bool
     * @throws GeneralException
     */
    public function unconfirm(Model $user)
    {
        if ($user->confirmed == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.not_confirmed'));
        }

        if ($user->id == 1) {
            // Cant un-confirm admin
            throw new GeneralException(trans('exceptions.backend.access.users.cant_unconfirm_admin'));
        }

        if ($user->id == access()->id()) {
            // Cant un-confirm self
            throw new GeneralException(trans('exceptions.backend.access.users.cant_unconfirm_self'));
        }

        $user->confirmed = 0;
        $unconfirmed = $user->save();

        if ($unconfirmed) {
            event(new UserUnconfirmed($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.cant_unconfirm')); // TODO
    }

    /**
     * @param  $input
     * @param  $user
     *
     * @throws GeneralException
     */
    protected function checkUserByEmail($input, $user)
    {
        //Figure out if email is not the same
        if ($user->email != $input['email']) {
            //Check to see if email exists
            if ($this->query()->where('email', '=', $input['email'])->first()) {
                throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
            }
        }
    }

    /**
     * @param $roles
     * @param $user
     */
    protected function flushRoles($roles, $user)
    {
        //Flush roles out, then add array of new ones
        $user->detachRoles($user->roles);
        $user->attachRoles($roles['assignees_roles']);
    }

    /**
     * @param  $roles
     *
     * @throws GeneralException
     */
    protected function checkUserRolesCount($roles)
    {
        //User Updated, Update Roles
        //Validate that there's at least one role chosen
        if (count($roles['assignees_roles']) == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.role_needed'));
        }
    }

    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserStub($input)
    {
        $user = self::MODEL;
        $user = new $user;
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        // 27 Oct 2017 Add start
        $user->username = $input['username'];
        $user->state_assignment = $input['state_assignment'];
        // 27 Oct 2017 Add end
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->status = isset($input['status']) ? 1 : 0;
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->confirmed = isset($input['confirmed']) ? 1 : 0;

        return $user;
    }


     /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserCreditsStub($user,$id)
    {
        
        $user_subscription             = self::MODELSUB;
        $user_subscription             = new $user_subscription;

        $subscriptions                 = $user_subscription->find($id);
        
        $subscription                  = self::MODELUS;
        $subscription                  = new $subscription;

        $subscription->subscription_id = $subscriptions->id;

        $subscription->user_id         = $user->id;
        $subscription->transactions_id = '';
        $subscription->listing_quantity= $subscriptions->listing_quantity;
        $subscription->images_quantity = $subscriptions->images_quantity;
        $subscription->claim_quantity  = $subscriptions->claim_quantity;
        $subscription->quantity        = 1;
        $trialEnd                      = date('Y-m-d', strtotime("+".$subscriptions->duration." days"));
        $expiry_date                   = $trialEnd;
        $subscription->expiry_date     = $expiry_date;
        $subscription->created_by      = access()->id();

        return $subscription;
    }

     /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserPorchdAccountStub($user,$id)
    {

        $user_subscription             = self::MODELSUB;
        $user_subscription             = new $user_subscription;
        
        $subscriptions                 = $user_subscription->find($id);
        $porchd_account                = self::MODELUPA;
        $porchd_account                = new $porchd_account;
        
        $porchd_account->allowed_porch = $subscriptions->listing_quantity;
        $porchd_account->user_id       = $user->id;
        $porchd_account->allowed_images= $subscriptions->images_quantity;
        $porchd_account->allowed_claim = $subscriptions->claim_quantity;
        
        return $porchd_account;
    }


}
