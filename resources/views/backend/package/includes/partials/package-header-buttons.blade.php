<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.package.index', trans('menus.backend.package.all'), [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.package.create', trans('menus.backend.package.create'), [], ['class' => 'btn btn-success btn-xs']) }}
    {{ link_to_route('admin.package.deactivated', trans('menus.backend.package.deactivated'), [], ['class' => 'btn btn-warning btn-xs']) }}
    {{ link_to_route('admin.package.deleted', trans('menus.backend.package.deleted'), [], ['class' => 'btn btn-danger btn-xs']) }}
</div><!--pull right-->

<div class="pull-right mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.package.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.package.index', trans('menus.backend.package.all')) }}</li>
            <li>{{ link_to_route('admin.package.create', trans('menus.backend.package.create')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('admin.package.deactivated', trans('menus.backend.package.deactivated')) }}</li>
            <li>{{ link_to_route('admin.package.deleted', trans('menus.backend.package.deleted')) }}</li>
        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>