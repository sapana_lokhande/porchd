<?php namespace App\Models\UserSubscription;

/**
 * Class UserSubscription
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UserSubscription\Traits\Relationship\UserSubscriptionRelationship;

class UserSubscription extends Model
{

    use SoftDeletes,
        UserSubscriptionRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'user_subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'subscription_id','user_id','transactions_id','listing_quantity','images_quantity','claim_quantity','expiry_date','created_by'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = [];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {   
        parent::__construct($attributes);
     
        $this->table = config('backend.user_subscriptions_table');
    }

}
