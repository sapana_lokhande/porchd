<?php

namespace App\Repositories\Backend\Access\Permission;

use App\Repositories\BaseRepository;
use App\Models\Access\Permission\PermissionGroup;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
/**
 * Class PermissionRepository.
 */
class PermissionGroupRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PermissionGroup::class;

    public function getAll($order_by = 'name', $sort = 'asc')
    {
        return $this->query()
            ->orderBy($order_by, $sort)
            ->get();
    }

    public function getGroups($order_by = 'name', $sort = 'asc')
    {
        return $this->query()
            ->orderBy($order_by, $sort)
            ->pluck('name','id');
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('access.permissiongroups_table').'.id',
                config('access.permissiongroups_table').'.name',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        // if ($this->query()->where('name', $input['name'])->first()) {
        //     throw new GeneralException(trans('exceptions.backend.access.permissions.already_exists'));
        // }

        // DB::transaction(function () use ($input) {
        //     $permission = self::MODEL;
        //     $permission = new $permission();
        //     $permission->name = $input['name'];
        //     $permission->display_name = $input['display_name'];
        //     $permission->sort = isset($input['sort']) && strlen($input['sort']) > 0 && is_numeric($input['sort']) ? (int) $input['sort'] : 0;

        //     //See if this role has all permissions and set the flag on the role
            
        //     if ($permission->save()) {
        //         //event(new RoleCreated($permission));
        //        return true;
        //     }

        //     throw new GeneralException(trans('exceptions.backend.access.permissions.create_error'));
        // });
    }

    /**
     * @param Model $permission
     * @param  $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function update(Permission $permission, array $input)
    {

      //   $permission->name = $input['name'];
      //   $permission->display_name = $input['display_name'];
      //   $permission->sort = isset($input['sort']) && strlen($input['sort']) > 0 && is_numeric($input['sort']) ? (int) $input['sort'] : 0;

      //  DB::transaction(function () use ($permission, $input) {
      //       if ($permission->save()) 
      //       {
      //           //event(new RoleUpdated($permission));
				  // return true;
      //       }

      //       throw new GeneralException(trans('exceptions.backend.access.permissions.update_error'));
      //   });
    }

    /**
     * @param Model $role
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Permission $permission)
    {
        //Would be stupid to delete the administrator role
        // if ($permission->id == 1) { //id is 1 because of the seeder
        //     throw new GeneralException(trans('exceptions.backend.access.permissions.cant_delete_admin'));
        // }
       
        // //Don't delete the role is there are users associated
        // if ($permission->roles()->count() > 0) {
        //     throw new GeneralException(trans('exceptions.backend.access.permissions.has_users'));
        // }

        // DB::transaction(function () use ($permission) {
        //     //Detach all associated roles
        //     $permission->roles()->sync([]);

        //     if ($permission->delete()) {
        //         //event(new RoleDeleted($role));

        //         return true;
        //     }

        //     throw new GeneralException(trans('exceptions.backend.access.permissions.delete_error'));
        // });
    }

    /**
     * @return mixed
     */
    public function getDefaultUserRole()
    {
        // if (is_numeric(config('access.users.default_role'))) {
        //     return $this->query()->where('id', (int) config('access.users.default_role'))->first();
        // }

        // return $this->query()->where('name', config('access.users.default_role'))->first();
    }
}
