<?php namespace App\Http\Controllers\Frontend\PurchasePackage;

/**
 * Class CartController
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Models\Package\Package;
use \Cart as Cart;
use Validator;
use App\Http\Requests\Frontend\Package\StoreRequest;
use App\Repositories\Frontend\UserSubscription\UserSubscriptionRepository;


class CartController extends Controller
{

    /**UserSubscriptionRepository
     */
    protected $users_subscription;

   /**
     * @param UserSubscriptionRepository $users
     */
    public function __construct(UserSubscriptionRepository $users_subscription)
    {
        $this->users_subscription = $users_subscription;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('frontend.package.cart');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if (!$duplicates->isEmpty()) {

            return redirect('package/cart')->withSuccessMessage('Item is already in your cart!');
        }

        Cart::add($request->id ,$request->name, 1, $request->price, ['listing_quantity' => $request->listing_quantity,'images_quantity' => $request->images_quantity,'claim_quantity' => $request->claim_quantity,'duration' => $request->claim_quantity ])->associate('\App\Models\Package\Package');
        return redirect('package/cart')->withSuccessMessage('Item was added to your cart!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        // Validation on max quantity
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);

         if ($validator->fails()) {
            session()->flash('error_message', 'Quantity must be between 1 and 5.');
            return response()->json(['success' => false]);
         }

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');

        return response()->json(['success' => true]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return redirect('package/cart')->withSuccessMessage('Item has been removed!');
    }

    /**
     * Remove the resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function emptyCart()
    {
        Cart::destroy();
        return redirect('package/cart')->withSuccessMessage('Your cart has been cleared!');
    }

    /**
     * Switch item from shopping cart to wishlist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToWishlist($id)
    {
        $item = Cart::get($id);

        Cart::remove($id);

        $duplicates = Cart::instance('wishlist')->search(function ($cartItem, $rowId) use ($id) {
            return $cartItem->id === $id;
        });

        if (!$duplicates->isEmpty()) {
            return redirect('package/cart')->withSuccessMessage('Item is already in your Wishlist!');
        }

        Cart::instance('wishlist')->add($item->id, $item->name, 1, $item->price)
                                  ->associate('App\Product');

        return redirect('cart')->withSuccessMessage('Item has been moved to your Wishlist!');

    }

     /**
     * Show the form for creating a new subscription.
     *
     * @return Response
     */
    public function proceed_checkout()
    {
        $items = Cart::content();
        return view('frontend.package.checkout')->withItems($items);
    }


     /**
     * Payment process.
     *
     * @return Response
     */
    public function payment(StoreRequest $request)
    {
       
        try 
        {
           
            $secretKey = config('app.secret_key');

            \Stripe\Stripe::setApiKey($secretKey);

            $token = $_POST['stripeToken'];

            // Create a Customer:
            $customer = \Stripe\Customer::create(array(
              "email" => access()->user()->email,
              "source" => $token,
            ));

            $stripe_request = array(
              "amount"   =>  round($request->amount*100),
              "currency" => "usd",
              "customer" => $customer->id
            );
           
            // Charge the Customer instead of the card:
            $charge = \Stripe\Charge::create($stripe_request);

            $chargeId = $charge->id;
            if(isset($chargeId) && !empty($chargeId))
            {
                //package details
                $items = Cart::content();
                //user details
                $userId = access()->user()->id;
                //payment details

                $payment_info = array(
                    'card_type'           => $request->credit_card_type,
                    'credit_card'         => $request->card_number,
                    'exp_year'            => $request->year,
                    'exp_month'           => $request->month,
                    'customer_profile_id' => $customer->id,
                    'name'                => $request->name,
                    'userId'              => $userId,
                ); 

                $transactions = array(
                    'transaction_id' => $chargeId,
                    'request'        => json_encode($stripe_request),
                    'response'       => $charge,
                    'userId'         => $userId,
                );

                $this->users_subscription->create([
                    'items'        => $items,
                    'userId'       => $userId,
                    'payment_info' => $payment_info,
                    'transactions' => $transactions
                    ]);
            }

            Cart::destroy();
            
            return redirect('package')->withSuccessMessage('Thank you for your order. Now it is time to upload your porchd.');
            
        } 
        catch ( \Exception $e ) 
        {
           
           $error = $e->getMessage();
           session()->flash('error_message', $error);
           return response()->json(['success' => false]);
           
        }
    }

}
