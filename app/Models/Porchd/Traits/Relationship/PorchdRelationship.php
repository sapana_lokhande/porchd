<?php

namespace App\Models\Porchd\Traits\Relationship;

/**
 * Class PorchdRelationship.
 */
trait PorchdRelationship
{
    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User','user_id','id');
    }

    public function porchdImages()
    {
        return $this->hasMany('App\Models\Porchd\PorchdImage','porchd_id','id');
    }

    public function porchdClaim()
    {
        return $this->hasOne('App\Models\Porchd\PorchdClaim','porchd_id');
    }
}
