<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */
    'frontend' =>[
        'package' => [
            'add_to_cart' => 'Add to Cart',
            'continue_shopping' => 'Continue Shopping',
            'proceed_checkout' => 'Proceed to Checkout',
            'remove' => 'Remove',
            'empty_cart'=> 'Empty Cart'
        ]
    ],


    'backend' => [
        'access' => [
            'users' => [
                'activate'           => 'Activate',
                'change_password'    => 'Change Password',
                'clear_session'         => 'Clear Session',
                'confirm'             => 'Confirm',
                'deactivate'         => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'login_as'           => 'Login As :user',
                'resend_email'       => 'Resend Confirmation E-mail',
                'restore_user'       => 'Restore User',
                'unconfirm'             => 'Un-confirm',
                'unlink' => 'Unlink',
            ],
        ],
        'package' => [
             'activate'     => 'Activate',
             'deactivate'   => 'Deactivate',
        ],
        'advertiser' => [
            'activate'           => 'Activate',
            'deactivate'         => 'Deactivate',
            'delete_permanently' => 'Delete Permanently',
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirm Account',
            'reset_password'  => 'Reset Password',
        ],
    ],

    'general' => [
        'cancel' => 'Cancel',
        'continue' => 'Continue',

        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit'   => 'Edit',
            'update' => 'Update',
            'view'   => 'View',
        ],

        'save' => 'Save',
        'view' => 'View',
    ],

    'porchd'    =>  [
        'next_step' =>  'Next Step',
        'previous_step' =>  'Previous Step'
    ]
];
