<?php namespace App\Repositories\Frontend\UserSubscription;

/**
 * Class UserSubscriptionRepository.
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 * @package App\Repositories\UserSubscription
 */

use App\Models\Access\User\User;
use App\Models\UserSubscription\UserSubscription;
use App\Models\UserPorchdAccount\UserPorchdAccount;
use App\Models\UserPaymentInfo\UserPaymentInfo;
use App\Models\Transaction\Transaction;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSubscriptionRepository.
 */
class UserSubscriptionRepository extends BaseRepository
{
     /**
     * Associated Repository Model.
     */
    const MODEL    = UserSubscription::class;
    const MODELUPA = UserPorchdAccount::class;
    const MODELUPI = UserPaymentInfo::class;
    const MODELT   = Transaction::class;
    const MODELUSER= User::class;

    /**
     * @var Model
     */
    protected $model;
    protected $modelupa;
    protected $modelupi;
    protected $modelt;


    /**
     * Construct
     *
     */
    public function __construct()
    {
        $this->model = new UserSubscription();
        $this->modelupa = new UserPorchdAccount();
        $this->modelupi = new UserPaymentInfo();
        $this->modelt = new Transaction();

    }

    /**
     * @param array $data
     * @param bool  $provider
     *
     * @return static
     */
    public function create(array $data, $provider = false)
    {

        $total_subscription = array();
        
        $allowed_porch = 0;
        $allowed_images = 0;
        $allowed_claim = 0;
       
        foreach ($data['items'] as $key => $value) 
        {
           
            $total_subscription['subscription_id']  = $value->id;
            $total_subscription['user_id']          = $data['userId'];
            $total_subscription['transactions_id']  = $data['transactions']['transaction_id'];
            $total_subscription['quantity']         = $value->qty;
          
            $trialEnd                               = date('Y-m-d', strtotime("+".$value->options->duration." days"));
            $total_subscription['expiry_date']      = $trialEnd;

            $listing_quantity                       = $value->options->listing_quantity * $value->qty;
            $total_subscription['listing_quantity'] = $listing_quantity;
            
            $images_quantity                        = $value->options->images_quantity * $value->qty;
            $total_subscription['images_quantity']  = $images_quantity;
            
            $claim_quantity                         = $value->options->claim_quantity * $value->qty;
            $total_subscription['claim_quantity']   = $claim_quantity;

            
            $allowed_porch                          = $allowed_porch + $listing_quantity;
            $allowed_images                         = $allowed_images + $images_quantity;
            $allowed_claim                          = $allowed_claim + $claim_quantity;

            $user_subscription                      = $this->createUserSubscriptionStub($total_subscription);
            
            DB::transaction(function () use ($user_subscription) {

                    $user_subscription->save();
             });

        }
         
    
        $user_porchd_account = $this->createUserPorchdAccountStub($allowed_porch,$allowed_images,$allowed_claim,$data['userId']);
        DB::transaction(function () use ($user_porchd_account) {

                $user_porchd_account->save();
        });

        $user_payment_info   = $this->createUserPaymentInfoStub($data['payment_info']);
        DB::transaction(function () use ($user_payment_info) {
               
                $user_payment_info->save();
        });
      
        $transactions = $this->createTransactionsStub($data['transactions']);
        DB::transaction(function () use ($transactions) {
                $transactions->save();
        });
        

        /*
         * Return the user object
         */
        return true;
    }

    
    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserSubscriptionStub($total_subscription_data)
    {

        $user_subscription = self::MODEL;
        $user_subscription = new $user_subscription;
   
        $user_subscription->subscription_id = $total_subscription_data['subscription_id'];
        $user_subscription->user_id = $total_subscription_data['user_id'];
        $user_subscription->transactions_id = $total_subscription_data['transactions_id'];
        $user_subscription->listing_quantity = $total_subscription_data['listing_quantity'];
        $user_subscription->images_quantity = $total_subscription_data['images_quantity'];
        $user_subscription->claim_quantity = $total_subscription_data['claim_quantity'];
        $user_subscription->expiry_date = $total_subscription_data['expiry_date'];
        $user_subscription->quantity = $total_subscription_data['quantity'];
        $user_subscription->created_by = $total_subscription_data['user_id'];

        return $user_subscription;
    }


    /**
     * @param  $input
     *
     * @return mixed
     */

    protected function createUserPorchdAccountStub($allowed_porch,$allowed_images,$allowed_claim,$userId)
    {
        $user_porchd_account                 = self::MODELUPA;
        $user_porchd_account                 = new $user_porchd_account;
        $user_porchd_account->allowed_porch  = $allowed_porch;
        $user_porchd_account->user_id        = $userId;
        $user_porchd_account->allowed_images = $allowed_images;
        $user_porchd_account->allowed_claim  = $allowed_claim;
        return $user_porchd_account;
    }

     /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserPaymentInfoStub($input)
    {
        $user_payment_info                      = self::MODELUPI;
        $user_payment_info                      = new $user_payment_info;
        $user_payment_info->customer_profile_id = $input['customer_profile_id'];
        $user_payment_info->user_id             = $input['userId'];
        $user_payment_info->name                = $input['name'];
        $user_payment_info->credit_card         = $input['credit_card'];
        $user_payment_info->exp_month           = $input['exp_month'];
        $user_payment_info->exp_year            = $input['exp_year'];
        $user_payment_info->card_type           = $input['card_type'];

        return $user_payment_info;
    }

    /**
     * CreateTransactionsStub
     * @param  $input
     *
     * @return mixed
     */
    protected function createTransactionsStub($input)
    {
        $transactions                   = self::MODELT;
        $transactions                   = new $transactions;
        $transactions->transaction_id   = $input['transaction_id'];
        $transactions->user_id          = $input['userId'];
        $transactions->request          = $input['request'];
        $transactions->response         = json_encode($input['response']);

        return $transactions;
    }

    /**
     * GetUserAssigendPackageByAdmin
     * @param  $input
     *
     * @return mixed
     */

    public function getUserAssigendPackageByAdmin($id,$loginUserId)
    {
       
       $user         = self::MODELUSER;
       $user         = new $user;
       $user         = $user->find($id);
       $subscription = $user->subscription()->where('created_by', '=' , $loginUserId)->pluck('subscription_id')->all();
       return $subscription;

    }
    
}
