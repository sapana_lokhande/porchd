<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
        *Back End Repository Listing
        *
        **/
        // Porchd
        $this->app->bind
        (
            \App\Repositories\Backend\Porchd\PorchdRepositoryContract::class,
            \App\Repositories\Backend\Porchd\EloquentPorchdRepository::class
        );
        // Advertiser
        $this->app->bind
        (
            \App\Repositories\Backend\Package\PackageRepositoryContract::class,
            \App\Repositories\Backend\Package\EloquentPackageRepository::class
        );
        
        /**
        *Back End Repository Listing
        *
        **/
        // Advertiser
        $this->app->bind
        (
            \App\Repositories\Backend\Advertiser\AdvertiserRepositoryContract::class,
            \App\Repositories\Backend\Advertiser\EloquentAdvertiserRepository::class
        );
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
