<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.advertiser.index', trans('menus.backend.advertiser.all'), [], ['class' => 'btn btn-primary btn-xs']) }}
    {{ link_to_route('admin.advertiser.create', trans('menus.backend.advertiser.create'), [], ['class' => 'btn btn-success btn-xs']) }}
    {{ link_to_route('admin.advertiser.deactivated', trans('menus.backend.advertiser.deactivated'), [], ['class' => 'btn btn-warning btn-xs']) }}
    {{ link_to_route('admin.advertiser.deleted', trans('menus.backend.advertiser.deleted'), [], ['class' => 'btn btn-danger btn-xs']) }}
</div><!--pull right-->

<div class="pull-right mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.advertiser.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.advertiser.index', trans('menus.backend.advertiser.all')) }}</li>
            <li>{{ link_to_route('admin.advertiser.create', trans('menus.backend.advertiser.create')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('admin.advertiser.deactivated', trans('menus.backend.advertiser.deactivated')) }}</li>
            <li>{{ link_to_route('admin.advertiser.deleted', trans('menus.backend.advertiser.deleted')) }}</li>
        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>