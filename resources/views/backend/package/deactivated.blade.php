@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.advertiser.management') . ' | ' . trans('labels.backend.advertiser.deactivated'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.package.management') }}
        <small>{{ trans('labels.backend.package.deactivated') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.package.deactivated') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.package.includes.partials.package-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="package-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.package.table.id') }}</th>
                            <th>{{ trans('labels.backend.package.table.name') }}</th>
                            <th>{{ trans('labels.backend.package.table.price') }}</th>
                            <th>{{ trans('labels.backend.package.table.created_at') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $('#package-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.package.get") }}',
                    type: 'post',
                    data: {status: 0, trashed: false},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'id', name: '{{config('package.subscriptions_table')}}.id'},
                    {data: 'name', name: '{{config('package.subscriptions_table')}}.name'},
                    {data: 'created_at', name: '{{config('package.subscriptions_table')}}.created_at'},
                    {data: 'updated_at', name: '{{config('package.subscriptions_table')}}.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection
