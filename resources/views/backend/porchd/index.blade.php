@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.porchd.porchd_managment') }}
        <small>{{ trans('labels.backend.porchd.active_porchd') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.porchd.active_porchd') }}</h3>

            <div class="box-tools pull-right">
                {{--  @include('backend.access.includes.partials.user-header-buttons')  --}}
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.backend.porchd.table.id') }}</th>
                        <th>{{ trans('labels.backend.porchd.table.label') }}</th>
                        <th>{{ trans('labels.backend.porchd.table.description') }}</th>
                        <th>{{ trans('labels.backend.porchd.table.state') }}</th>
                       <th>{{ trans('labels.backend.porchd.table.city') }}</th>
                       <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('User') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function () {
            $('#users-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.porchd.get") }}',
                    type: 'post',
                    data: {status: 1, trashed: false}
                    
                },
                columns: [
                    {data: 'id', name: '{{config('access.porchd_table')}}.id'},
                    {data: 'name', name: '{{config('access.porchd_table')}}.name'},
                    {data: 'description', name: '{{config('access.porchd_table')}}.description'},
                    {data: 'state', name: '{{config('access.porchd_table')}}.state'},
                    {data: 'city', name: '{{config('access.porchd_table')}}.city'},
                      {data: 'actions', name: 'actions', searchable: false, sortable: false}
                   
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection
