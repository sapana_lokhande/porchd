<?php

/**
* Package
*/
Route::group(['namespace' => 'Package'], function () {
     Route::any('package/test',function(){
        dd(1);
     });

    /*
     * For DataTables
     */
    Route::post('package/get', 'PackageController')->name('package.get');

    /*
     * Package Status'
     */
    Route::get('package/deactivated', 'PackageController@getDeactivated')->name('package.deactivated');
    Route::get('package/deleted', 'PackageController@getDeleted')->name('package.deleted');

    /*
     * Package CRUD
     */
    Route::resource('package', 'PackageController');

    // Active
    Route::get('package/mark/{id}/{status}', 'PackageController@mark')->name('package.mark')->where(['active' => '[0,1]']); 

    /*
     * Deleted Package
     */
    Route::get('package/delete/{id}', 'PackageController@delete')->name('package.delete-permanently');
    Route::get('package/restore/{id}', 'PackageController@restore')->name('package.restore');

});
