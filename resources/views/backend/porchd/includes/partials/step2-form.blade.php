<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
<div class="container">
	<div class="row">
		<div class="col-md-6">
		
            <div class ="col-md-3" ng-repeat="x in porchd_image_list" ng-if="porchd_image_list">
                <img src ="{{ asset('images/2/Thumbnail')}}/@{{x.name}}"></img>
            </div>
        </div>
		<div class="col-md-6">
			{!! Form::open([ 'route' =>['admin.porchd.addimage',$porchd->id??null], 'files' => true, 'enctype' => 'multipart/form-data',
			'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
			<div>
				<h3>Upload Multiple Image By Click On Box</h3>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<script type="text/javascript">
	Dropzone.options.imageUpload = {
			addRemoveLinks: true,
            maxFilesize         :       1,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
             success: function(file, response){
                $.ajax({
                    method: "POST",
                    url: "admin/access/porchd/imagelist",
                    data: {id:'{{$porchd->id??null}}' }
                    })
                    .done(function( msg ) {
                        alert( "Data Saved: " + msg );
                    });
            }
        };

</script>