<?php namespace App\Repositories\Backend\Package;

/**
 * Class EloquentPackageRepository
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 * @package App\Repositories\Backend\Package
 */

use App\Models\Package\Package;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Events\Backend\Package\PackageDeactivated;
use App\Events\Backend\Package\PackageReactivated;
use App\Repositories\Backend\Package\PackageRepositoryContract;

/**
 * Class EloquentPackageRepository.
 */
class EloquentPackageRepository extends BaseRepository implements PackageRepositoryContract
{

   /**
     * Associated Repository Model.
     */
    const MODEL = Package::class;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Construct
     *
     */
    public function __construct()
    {
        $this->model = new Package();
    }
    
    /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->select([
                config('backend.subscriptions_table').'.id',
                config('backend.subscriptions_table').'.name',
                config('backend.subscriptions_table').'.price',
                config('backend.subscriptions_table').'.active',
                config('backend.subscriptions_table').'.created_at',
                config('backend.subscriptions_table').'.updated_at',
                config('backend.subscriptions_table').'.deleted_at',
            ]);

        if ($trashed == 'true') 
        {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    /**
     * Create
     *
     * @param array $input
     */
    public function create($input)
    {
        $data       = $input['data'];
        $package    = $this->createPackageStub($data);

        $this->checkPackageByName($data, $package);

        DB::transaction(function () use ($package, $data) 
        {
            if ($package->save()) 
            {
                // event(new AdvertiserCreated($advertiser));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.package.create_error'));
        });
    }

    /**
     * Update
     *
     * @param Model $package
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $package, array $input)
    {
        $data = $input['data'];

        $package->name             = $data['name'];
        $package->description      = $data['description'];
        $package->user_type        = $data['user_type'];
        $package->listing_quantity = $data['listing_quantity'];
        $package->images_quantity  = $data['images_quantity'];
        $package->claim_quantity   = $data['claim_quantity'];
        $package->description      = $data['description'];
        $package->price            = $data['price'];
        $package->active           = isset($data['active']) ? 1 : 0;

        DB::transaction(function () use ($package, $data) 
        {
            if ($package->save()) 
            {

                // event(new AdvertiserUpdated($advertiser));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.package.update_error'));
        });
    }

    /**
     * Delete
     *
     * @param Model $package
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Model $package)
    {
        if ($package->delete()) 
        {
            // event(new AdvertiserDeleted($advertiser));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.package.delete_error'));
    }

    /**
     * Force Delete
     *
     * @param Model $package
     *
     * @throws GeneralException
     */
    public function forceDelete(Model $package)
    {
        if (is_null($package->deleted_at)) 
        {
            throw new GeneralException(trans('exceptions.backend.package.delete_first'));
        }

        DB::transaction(function () use ($package) 
        {
            if ($package->forceDelete()) 
            {
                // event(new AdvertiserPermanentlyDeleted($advertiser));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.advertiser.delete_error'));
        });
    }

    /**
     * Restore
     *
     * @param Model $package
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function restore(Model $package)
    {
        if (is_null($package->deleted_at)) 
        {
            throw new GeneralException(trans('exceptions.backend.package.cant_restore'));
        }

        if ($package->restore()) 
        {
            // event(new AdvertiserRestored($advertiser));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.package.restore_error'));
    }

    /**
     * Mark
     *
     * @param Model $package
     * @param $status
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function mark(Model $package, $status)
    {
        $package->active = $status;

        switch ($status) 
        {
            case 0:
                event(new PackageDeactivated($package));
            break;

            case 1:
                event(new PackageReactivated($package));
            break;
        }

        if ($package->save()) 
        {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.package.mark_error'));
    }

    /**
     * Check Package By Name 
     *
     * @param  $input
     * @param  $advertiser
     *
     * @throws GeneralException
     */
    protected function checkPackageByName($input, $package)
    {
        //Figure out if name is not the same
        if ($package->name != $input['name']) 
        {
            //Check to see if name exists
            if ($this->query()->where('name', '=', $input['name'])->first()) 
            {
                throw new GeneralException(trans('exceptions.backend.package.name'));
            }
        }
    }

    /**
     * Create Package Stub
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    protected function createPackageStub($input)
    {

        $this->model->name             = $input['name'];
        $this->model->description      = $input['description'];
        $this->model->user_type        = $input['user_type'];
        $this->model->listing_quantity = $input['listing_quantity'];
        $this->model->images_quantity  = $input['images_quantity'];
        $this->model->claim_quantity   = $input['claim_quantity'];
        $this->model->description      = $input['description'];
        $this->model->price            = $input['price'];
        $this->model->active           = isset($input['active']) ? 1 : 0;

        return $this->model;
    }

    public function getPackage($order_by = 'name', $sort = 'asc')
    {
        return $this->query()
            ->orderBy($order_by, $sort)
            ->pluck('name','id');
    }
}