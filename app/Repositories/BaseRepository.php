<?php namespace App\Repositories;

/**
 * Class BaseRepository.
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 * @package App\Repositories\BaseRepository
 */
class BaseRepository
{
    /**
     * @return mixed
     */
    public function getAll($orderBy = 'id', $sort = 'asc')
    {
        return $this->model->orderBy($orderBy, $sort)->get();
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->query()->count();
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->query()->find($id);
    }

    /**
     * @return mixed
     */
    public function query()
    {
        return call_user_func(static::MODEL.'::query');
    }

     /**
     * Select All
     *
     * @param string $columns
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function selectAll($columns='*', $order_by = 'id', $sort = 'asc')
    {
        return $this->model->select($columns)->orderBy($order_by, $sort)->get();
    }

    /**
     * Destroy Item
     *
     * @param $id
     * @return bool
     * @throws GeneralException
     */
    public function destroy($id)
    {
        if($this->model->where('ID', '=', $id)->delete())
        {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.delete_error'));
    }

     /**
     * Get Paginated
     *
     * @param $per_page
     * @param string $active
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getPaginated($per_page, $active = '', $order_by = 'id', $sort = 'asc')
    {
        if($active)
        {
            return $this->model->where('status', $active)
                ->orderBy($order_by, $sort)
                ->paginate($per_page);
        }
        else
        {
            return $this->model->orderBy($order_by, $sort)
                ->paginate($per_page);
        }
    }

    /**
     * Find Or Throw Exception
     *
     * @param $id
     * @param array $relations
     * @return mixed
     * @throws GeneralException
     */
    public function findOrThrowException($id, $relations = array())
    {
        if(is_numeric($id))
        {
            try
            {
                if(is_array($relations) && !empty($relations))
                {
                    $foundModel = $this->model->with($relations)->find($id);
                }
                else
                {
                    $foundModel = $this->model->withTrashed()->find($id);
                }

                if(!is_null($foundModel) && isset($foundModel->id))
                {
                    return $foundModel;
                }
            }
            catch(ModelNotFoundException $e)
            {
                throw new GeneralException(trans('exceptions.backend.not_found'));
            }
        }

        throw new GeneralException(trans('exceptions.backend.not_found'));
    }

}
