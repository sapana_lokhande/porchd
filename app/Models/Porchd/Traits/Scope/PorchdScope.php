<?php

namespace App\Models\Porchd\Traits\Scope;

/**
 * Class PorchdScope.
 */
trait PorchdScope
{
    /**
     * @param $query
     * @param string $direction
     *
     * @return mixed
     */
    public function scopeSort($query, $direction = 'asc')
    {
        return $query->orderBy('sort', $direction);
    }

        /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = 1)
    {
        return $query->where('active', $status);
    }
}
