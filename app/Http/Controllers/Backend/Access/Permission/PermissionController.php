<?php

namespace App\Http\Controllers\Backend\Access\Permission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\Permission\PermissionRepository;
use App\Repositories\Backend\Access\Permission\PermissionGroupRepository;
use App\Http\Requests\Backend\Access\Permission\ManagePermission;
use App\Http\Requests\Backend\Access\Permission\StorePermissionRequest;
use App\Http\Requests\Backend\Access\Permission\UpdatePermissionRequest;
use App\Models\Access\Permission\Permission;

class PermissionController extends Controller
{
    
    /**
     * @var PermissionRepository
     */
    protected $permissions;
    protected $permissiongroup;

    /**
     * 
     * @param PermissionRepository $permissions
     */
    public function __construct(PermissionRepository $permissions,PermissionGroupRepository $permissiongroup)
    {
        $this->permissions = $permissions;
        $this->permissiongroup= $permissiongroup;
    }

    /**
     * @param ManageRoleRequest $request
     *
     * @return mixed
     */
    public function index()
    {

        return view('backend.access.permission.index');
    }

    /**
     * @param ManageRoleRequest $request
     *
     * @return mixed
     */
    public function create( ManagePermission $request)
    {
        return view('backend.access.permission.create')->withGroup($this->permissiongroup->getGroups());
    }

    /**
     * @param StoreRoleRequest $request
     *
     * @return mixed
     */
    public function store(StorePermissionRequest $request)
    {
        $this->permissions->create($request->only('name', 'display_name', 'sort','group_id'));

        return redirect()->route('admin.access.permission.index')->withFlashSuccess(trans('alerts.backend.permissions.created'));
    }

    /**
     * @param Permission        $permission
     * @param ManageRoleRequest $request
     *
     * @return mixed
     */
    public function edit(Permission $permission,ManagePermission $request)
    {
        return view('backend.access.permission.edit')
                ->withPermission($permission)
                ->withGroup($this->permissiongroup->getGroups());
    }

    /**
     * @param Role              $role
     * @param UpdateRoleRequest $request
     *
     * @return mixed
     */
    public function update(Permission $permission,UpdatePermissionRequest $request)
    {
        $this->permissions->update($permission, $request->only('name', 'display_name', 'sort','group_id'));

        return redirect()->route('admin.access.permission.index')->withFlashSuccess(trans('alerts.backend.permissions.updated'));
    }

    /**
     * @param Role              $role
     * @param ManageRoleRequest $request
     *
     * @return mixed
     */
    public function destroy(Permission $permission,ManagePermission $request)
    {
        $this->permissions->delete($permission);

        return redirect()->route('admin.access.permission.index')->withFlashSuccess(trans('alerts.backend.permissions.deleted'));
    }
}
