<?php namespace App\Repositories\Frontend\State;

/**
 * Class BaseRepository.
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 * @package App\Repositories\StateRepository
 */

use App\Models\State\State;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StateRepository.
 */
class StateRepository extends BaseRepository
{
     /**
     * Associated Repository Model.
     */
    const MODEL = State::class;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Construct
     *
     */
    public function __construct()
    {
        $this->model = new State();
    }
    
}
