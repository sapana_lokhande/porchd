<?php namespace App\Models\Advertiser\Traits\Scope;

/**
 * Class AdvertiserScope
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */

trait AdvertiserScope
{
    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
