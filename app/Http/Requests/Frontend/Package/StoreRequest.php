<?php namespace App\Http\Requests\Frontend\Package;

/**
 * Class StoreRequest
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_number'  => 'required|max:191',
            'card_code'   => 'required|max:191',
            'month'   => 'required|max:191',
            'year'   => 'required|max:191',
            'amount'   => 'required|max:191'
        ];
    }
}
