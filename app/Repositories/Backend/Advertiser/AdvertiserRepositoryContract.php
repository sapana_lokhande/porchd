<?php namespace App\Repositories\Backend\Advertiser;

/**
 * Class AdvertiserRepositoryContract
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 * @package App\Repositories\Backend\Advertiser
 */

interface AdvertiserRepositoryContract
{
    /**
     * Find or Throw Exception
     *
     * @param $id
     * @return mixed
     */
    public function findOrThrowException($id);

    /**
     * Get Paginated
     *
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getPaginated($per_page, $order_by = 'id', $sort = 'asc');

    /**
     * Get All
     *
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc');

    /**
     * Update
     *
     * @param  $id
     * @param  $input
     * @return mixed
     */
     // public function update($id, $input);

    /**
     * Destroy
     *
     * @param  $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * Select All
     *
     * @param string $columns
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function selectAll($columns='*', $order_by = 'id', $sort = 'asc');

}