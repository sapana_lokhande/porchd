<?php namespace App\Repositories\Backend\Advertiser;

/**
 * Class EloquentAdvertiserRepository
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 * @package App\Repositories\Backend\Advertiser
 */

use App\Models\Advertiser\Advertiser;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Events\Backend\Advertiser\AdvertiserCreated;
use App\Events\Backend\Advertiser\AdvertiserDeleted;
use App\Events\Backend\Advertiser\AdvertiserUpdated;
use App\Events\Backend\Advertiser\AdvertiserRestored;
use App\Events\Backend\Advertiser\AdvertiserDeactivated;
use App\Events\Backend\Advertiser\AdvertiserReactivated;
use App\Events\Backend\Advertiser\AdvertiserPermanentlyDeleted;
use App\Repositories\Backend\Advertiser\AdvertiserRepositoryContract;

/**
 * Class EloquentAdvertiserRepository.
 */
class EloquentAdvertiserRepository extends BaseRepository implements AdvertiserRepositoryContract
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Advertiser::class;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Construct
     *
     */
    public function __construct()
    {
        $this->model = new Advertiser();
    }
    
    /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->select([
                config('backend.advertisers_table').'.id',
                config('backend.advertisers_table').'.image',
                config('backend.advertisers_table').'.name',
                config('backend.advertisers_table').'.url',
                config('backend.advertisers_table').'.active',
                config('backend.advertisers_table').'.created_at',
                config('backend.advertisers_table').'.updated_at',
                config('backend.advertisers_table').'.deleted_at',
            ]);

        if ($trashed == 'true') 
        {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    /**
     * Create
     *
     * @param array $input
     */
    public function create($input)
    {
        $data       = $input['data'];
        $advertiser = $this->createAdvertiserStub($data);

        $this->checkAdvertiserByName($data, $advertiser);

        DB::transaction(function () use ($advertiser, $data) 
        {
            if ($advertiser->save()) 
            {
                event(new AdvertiserCreated($advertiser));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.advertiser.create_error'));
        });
    }

    /**
     * Update
     *
     * @param Model $advertiser
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $advertiser, array $input)
    {
        $data = $input['data'];

        $advertiser->name     = $data['name'];
        $advertiser->url      = $data['url'];
        $advertiser->active   = isset($data['status']) ? 1 : 0;

        DB::transaction(function () use ($advertiser, $data) 
        {
            if ($advertiser->save()) 
            {

                event(new AdvertiserUpdated($advertiser));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.advertiser.update_error'));
        });
    }

    /**
     * Delete
     *
     * @param Model $advertiser
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Model $advertiser)
    {
        if ($advertiser->delete()) 
        {
            event(new AdvertiserDeleted($advertiser));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.advertiser.delete_error'));
    }

    /**
     * Force Delete
     *
     * @param Model $advertiser
     *
     * @throws GeneralException
     */
    public function forceDelete(Model $advertiser)
    {
        if (is_null($advertiser->deleted_at)) 
        {
            throw new GeneralException(trans('exceptions.backend.advertiser.delete_first'));
        }

        DB::transaction(function () use ($advertiser) 
        {
            if ($advertiser->forceDelete()) 
            {
                event(new AdvertiserPermanentlyDeleted($advertiser));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.advertiser.delete_error'));
        });
    }

    /**
     * Restore
     *
     * @param Model $advertiser
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function restore(Model $advertiser)
    {
        if (is_null($advertiser->deleted_at)) 
        {
            throw new GeneralException(trans('exceptions.backend.advertiser.cant_restore'));
        }

        if ($advertiser->restore()) 
        {
            event(new AdvertiserRestored($advertiser));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.advertiser.restore_error'));
    }

    /**
     * Mark
     *
     * @param Model $advertiser
     * @param $status
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function mark(Model $advertiser, $status)
    {
        $advertiser->active = $status;

        switch ($status) 
        {
            case 0:
                event(new AdvertiserDeactivated($advertiser));
            break;

            case 1:
                event(new AdvertiserReactivated($advertiser));
            break;
        }

        if ($advertiser->save()) 
        {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.advertiser.mark_error'));
    }

    /**
     * Check Advertiser By Name 
     *
     * @param  $input
     * @param  $advertiser
     *
     * @throws GeneralException
     */
    protected function checkAdvertiserByName($input, $advertiser)
    {
        //Figure out if name is not the same
        if ($advertiser->name != $input['name']) 
        {
            //Check to see if name exists
            if ($this->query()->where('name', '=', $input['name'])->first()) 
            {
                throw new GeneralException(trans('exceptions.backend.advertiser.name'));
            }
        }
    }

    /**
     * Create Advertiser Stub
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    protected function createAdvertiserStub($input)
    {
        $this->model->name      = $input['name'];
        $this->model->url       = $input['url'];
        $this->model->active    = isset($input['status']) ? 1 : 0;

        return $this->model;
    }
}
