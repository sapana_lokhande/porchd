<?php

return [

    /*
     * Skin for Admin LTE backend theme
     *
     * Available options:
     *
     * black
     * black-light
     * blue
     * blue-light
     * green
     * green-light
     * purple
     * purple-light
     * red
     * red-light
     * yellow
     * yellow-light
     */
    'theme' => 'blue',

    /*
     * Layout for the Admin LTE backend theme
     *
     * Fixed:               use the class .fixed to get a fixed header and sidebar.
     *                      This makes scrolling affect the content only and put the sidebar and header in a fixed position.
     *
     * Collapsed Sidebar:   use the class .sidebar-collapse to have a collapsed sidebar upon loading.
     *                      Use this if you want the sidebar to be hidden by default.
     *
     * Boxed Layout:        use the class .layout-boxed to get a boxed layout that stretches only to 1250px.
     *                      Provides spaces on both sides of the screen, if the screen is big enough.
     *
     * Top Navigation:      use the class .layout-top-nav to remove the sidebar and have your links at the top navbar.
     *                      Makes the sidebar hover the content when expanded.
     *
     * Sidebar Mini:        Shows the only the icons of the sidebar items when collapsed. Sidebar will not fully collapse.
     *
     * Available options:
     *
     * fixed
     * sidebar-collapse
     * layout-boxed
     * layout-top-nav
     * sidebar-mini
     *
     * Note: you cannot use both layout-boxed and fixed at the same time. Anything else can be mixed together.
     */
    'layout' => 'sidebar-mini',

    /*
     * Subscriptions table used to store subscriptions
     */

    'subscriptions_table' => 'subscriptions',

    /*
     * Advertisers table used to store advertisers
     */
    'advertisers_table' => 'advertisements',

    /*
     * Transaction table used to store transaction
     */
    'transactions_table' => 'transactions',

    /*
     * User Payment Info table used to user payment info
     */
    'user_payment_info_table' => 'user_payment_info',

    /*
     * User Porchd Account table used to user porchd account
     */
    'user_porchd_account_table' => 'user_porchd_account',

    /*
     * User Subscriptions table used to user subscriptions details
     */
    'user_subscriptions_table' => 'user_subscriptions',
];
