<?php namespace App\Http\Controllers\Frontend\PurchasePackage;

/**
 * Class PurchasePackagesController
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Http\Request;
use App\Models\Package\Package;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Package\PackageRepositoryContract;


class PurchasePackagesController extends Controller
{
    /**
     * AdvertiserRepositoryContract
     *
     * @var object
     */
    protected $repository;

     /**
     * Construct
     *
     * @param PackageRepositoryContract
     */
    public function __construct(PackageRepositoryContract $repository)
    {
          $this->repository = new $repository;
    }

    
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
       return view('frontend.package.package')->withPackages($this->repository->getAll());
    }




}
