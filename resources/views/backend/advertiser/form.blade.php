<div class="box box-success">
    <div class="box-header with-border">
        @if(isset($advertiser))
            <h3 class="box-title">{{ trans('labels.backend.advertiser.edit') }}</h3>
        @else
            <h3 class="box-title">{{ trans('labels.backend.advertiser.create') }}</h3>
        @endif

        <div class="box-tools pull-right">
            @include('backend.advertiser.includes.partials.advertiser-header-buttons')
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.advertiser.name'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.advertiser.name')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('url', trans('validation.attributes.backend.advertiser.url'),
            ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('url', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.advertiser.url')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('status', trans('validation.attributes.backend.advertiser.active'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-1">
                {{ Form::checkbox('status', '1', isset($advertiser) && $advertiser->status == 1) }}
            </div><!--col-lg-1-->
        </div><!--form control-->
    </div><!-- /.box-body -->
</div><!--box-->

<div class="box box-success">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.advertiser.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
        </div><!--pull-left-->

         @if(isset($advertiser))
            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-xs']) }}
            </div><!--pull-right-->
        @else
            <div class="pull-right">
                {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
            </div><!--pull-right-->
        @endif

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->