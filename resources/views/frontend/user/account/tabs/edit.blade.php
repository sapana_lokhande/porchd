{{ Form::model($logged_in_user, ['route' => 'frontend.user.profile.update', 'files'=>true, 'class' => 'form-horizontal', 'method' => 'PATCH']) }}

    <div class="form-group">
        {{ Form::label('first_name', trans('validation.attributes.frontend.first_name'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('first_name', null,
            ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('last_name', trans('validation.attributes.frontend.last_name'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('username', trans('validation.attributes.frontend.username'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('username', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.username')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('phone_number', trans('validation.attributes.frontend.phone_number'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('phone_number', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.phone_number')]) }}
        </div>
    </div>
    @role(2)
    <div class="form-group">
        {{ Form::label('brokerage_name', trans('validation.attributes.frontend.brokerage_name'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('brokerage_name', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.brokerage_name')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('address1', trans('validation.attributes.frontend.address1'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('address1', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.address1')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('address2', trans('validation.attributes.frontend.address2'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('address2', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.address2')]) }}
        </div>
    </div>
    <div class="form-group">
      <!--   {{ Form::label('country', trans('validation.attributes.frontend.country'),
        ['class' => 'col-md-4 control-label']) }} -->
        <div class="col-md-6">
            {{ Form::hidden('country', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.country')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('address2', trans('validation.attributes.frontend.state'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::select('state', $state, 'all', ['class' => 'form-control']) }}
           
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('city', trans('validation.attributes.frontend.city'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('city', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.city')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('zip', trans('validation.attributes.frontend.zip'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('zip', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.zip')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('state_assignment', trans('validation.attributes.frontend.state_assignment'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('state_assignment', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.state_assignment')]) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('license_number', trans('validation.attributes.frontend.license_number'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::text('license_number', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.license_number')]) }}
        </div>
    </div>
    
    @endauth
    <div class="form-group">
        {{ Form::label('avatar', trans('validation.attributes.frontend.avatar'),
        ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
             {!! Form::file('avatar', array('class' => 'form-control')) !!}
        </div>
    </div>
    @if ($logged_in_user->canChangeEmail())
        <div class="form-group">
            {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
            <div class="col-md-6">
                <div class="alert alert-info">
                    <i class="fa fa-info-circle"></i> {{  trans('strings.frontend.user.change_email_notice') }}
                </div>

                {{ Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
            </div>
        </div>
    @endif

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-primary', 'id' => 'update-profile']) }}
        </div>
    </div>

{{ Form::close() }}