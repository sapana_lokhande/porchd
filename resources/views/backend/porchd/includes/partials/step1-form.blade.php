<form class="form-horizontal" role="form" ng-submit="submitForm()" novalidate name="porchd_form" id="porchd_forms_step1">
	<div class="box box-success">
		<br/>
		<strong>Agent Details</strong>
		<hr>

		<div class="box-body">
			<div class="col-md-6">
				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'disabled' => 'disabled', 'autofocus'
						=> 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.users.first_name'), 'ng-model'=>'first_name'])
						}}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'disabled' => 'disabled', 'placeholder'
						=> trans('validation.attributes.backend.access.users.last_name'), 'ng-model'=>'last_name']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->


				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('organisation_name', null, ['class' => 'form-control', 'maxlength' => '191', 'disabled' => 'disabled', 'placeholder'
						=> trans('validation.attributes.backend.access.users.brokarage_name'), 'ng-model'=>'organisation_name']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'disabled' => 'disabled', 'placeholder' =>
						trans('validation.attributes.backend.access.users.email'), 'ng-model'=>'email']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('phone', null,['class' => 'form-control', 'disabled' => 'disabled', 'placeholder' => trans('validation.attributes.backend.access.users.mno'),
						'ng-model'=>'phone']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->
			</div>

			<div class="col-md-6">

				<div class="form-group" ng-if="!update" ng-class="{ 'has-error' : porchd_form.selectedUserTypes.$invalid && (porchd_form.$submitted || porchd_form.selectedUserTypes.$touched) }">
					<div class="col-lg-10">
						<select ng-model="selectedUserTypes" class="form-control" ng-change="getUsers(selectedUserTypes)" name="selectedUserTypes">
							<option value="">Select Role</option>
							<option ng-repeat="(id,name) in users_types" value="@{{id}}">@{{name}}</option>
						</select>
						<p ng-show="porchd_form.selectedUserTypes.$error.required && (porchd_form.$submitted || porchd_form.selectedUserTypes.$touched)"
						 class="help-block"></p>

					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-if="selectedUserTypes!=''">
					<div class="col-lg-10">
						{{-- {{ Form::select('users_list', [], '', ['class' => 'form-control','ng-option'=>'x for x in users_list','ng-model'=>"selectedName"])
						}} --}}
						<select ng-model="selectedUserName" class="form-control" ng-change="getUserData(selectedUserName)" required name="selectedUserName">
							<option value="">Select User</option>
							<option ng-repeat="(id,name) in users_list" value="@{{id}}">@{{name}}</option>
						</select>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->



				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('address1',null, ['class' => 'form-control', 'disabled' => 'disabled', 'placeholder' => trans('validation.attributes.frontend.address1'),
						'ng-model'=>'address1']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('address2',null, ['class' => 'form-control', 'disabled' => 'disabled', 'placeholder' => trans('validation.attributes.frontend.address2'),
						'ng-model'=>'address2']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('state',null, ['class' => 'form-control', 'disabled' => 'disabled', 'placeholder' => trans('validation.attributes.frontend.state'),
						'ng-model'=>'state']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->


				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('city',null, ['class' => 'form-control', 'disabled' => 'disabled', 'placeholder' => trans('validation.attributes.frontend.city'),
						'ng-model'=>'city']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group">
					<div class="col-lg-10">
						{{ Form::text('zip',null, ['class' => 'form-control','disabled' => 'disabled', 'placeholder' => trans('validation.attributes.frontend.zip'),
						'ng-model'=>'zip']) }}
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->


			</div>
			<!-- /.box-body -->
		</div>

		<br/>
		<strong>Proprty Details</strong>
		<hr>
		<div class="box-body">
			<div class="col-md-6">

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_address1.$invalid && (porchd_form.$submitted || porchd_form.porchd_address1.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_address1',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.address1'),
						'ng-model'=>'porchd_address1','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_address1.$error.required && (porchd_form.$submitted || porchd_form.porchd_address1.$touched)"
						 class="help-block"></p>
						<p ng-show="porchd_form.porchd_address1.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_address1.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_address2.$invalid && (porchd_form.$submitted || porchd_form.porchd_address2.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_address2',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.address2'),
						'ng-model'=>'porchd_address2','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_address2.$error.required && (porchd_form.$submitted || porchd_form.porchd_address2.$touched)"
						 class="help-block"></p>
						<p ng-show="porchd_form.porchd_address2.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_address2.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->



				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_state.$invalid && (porchd_form.$submitted || porchd_form.porchd_state.$touched) }">
					<div class="col-lg-10">
						{{-- {{ Form::text('porchd_state',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.state'),
						'ng-model'=>'porchd_state']) }} --}}

						<select ng-model="porchd_state" class="form-control" required name="porchd_state">
							<option value="">Select State</option>
							<option ng-repeat="(id,name) in state_list" value="@{{id}}" ng-selected="porchd_state==id">@{{name}}</option>
						</select>
						<p ng-show="porchd_form.porchd_state.$error.required && (porchd_form.$submitted || porchd_form.porchd_state.$touched)" class="help-block"></p>

					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_city.$invalid && (porchd_form.$submitted || porchd_form.porchd_city.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_city',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.city'),
						'ng-model'=>'porchd_city','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_city.$error.required && (porchd_form.$submitted || porchd_form.porchd_city.$touched)" class="help-block"></p>
						<p ng-show="porchd_form.porchd_city.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_city.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_zip.$invalid && (porchd_form.$submitted || porchd_form.porchd_zip.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_zip',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.zip'),
						'ng-model'=>'porchd_zip','ng-minlength'=>"6",'ng-maxlength'=>"6","type"=>"number"]) }}

						<p ng-show="porchd_form.porchd_zip.$error.required && (porchd_form.$submitted || porchd_form.porchd_zip.$touched)" class="help-block"></p>
						<p ng-show="porchd_form.porchd_zip.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_zip.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_label.$invalid && (porchd_form.$submitted || porchd_form.porchd_label.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_label',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.label'),
						'ng-model'=>'porchd_label','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_label.$error.required && (porchd_form.$submitted || porchd_form.porchd_label.$touched)" class="help-block"></p>
						<p ng-show="porchd_form.porchd_label.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_label.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_sqfeet.$invalid && (porchd_form.$submitted || porchd_form.porchd_sqfeet.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_sqfeet',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.sqfeet'),
						'ng-model'=>'porchd_sqfeet','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_sqfeet.$error.required && (porchd_form.$submitted || porchd_form.porchd_sqfeet.$touched)"
						 class="help-block"></p>
						<p ng-show="porchd_form.porchd_sqfeet.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_sqfeet.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_bedroom.$invalid && (porchd_form.$submitted || porchd_form.porchd_bedroom.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_bedroom',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.bedroom'),
						'ng-model'=>'porchd_bedroom','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_bedroom.$error.required && (porchd_form.$submitted || porchd_form.porchd_bedroom.$touched)"
						 class="help-block"></p>
						<p ng-show="porchd_form.porchd_bedroom.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_bedroom.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_bathroom.$invalid && (porchd_form.$submitted || porchd_form.porchd_bathroom.$touched) }">
					<div class="col-lg-10">
						{{ Form::text('porchd_bathroom',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.bedroom'),
						'ng-model'=>'porchd_bathroom','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_bathroom.$error.required && (porchd_form.$submitted || porchd_form.porchd_bathroom.$touched)"
						 class="help-block"></p>
						<p ng-show="porchd_form.porchd_bathroom.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_bathroom.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->

				<div class="form-group">
					<div class="col-lg-1">
						{{ Form::checkbox('porchd_inquiry', '1', true,['class' => 'col-lg-12 pull-left','ng-checked'=>'porchd_inquiry==1' ]) }}
					</div>
					{{ Form::label('porchd_inquiry', trans('validation.attributes.frontend.porchd_inquiry'), ['class' => 'col-lg-11 control-label
					pull-left']) }}

					<!--col-lg-1-->
				</div>
				<!--form control-->

				<div class="pull-right">
					{{ Form::button(trans('buttons.porchd.next_step'), ['class' => 'btn btn-success btn-lg','type'=>'submit']) }}
				</div>
			</div>
			<!-- /.box-body -->

			<div class="col-md-6">
				<div class="form-group" ng-class="{ 'has-error' : porchd_form.porchd_description.$invalid && (porchd_form.$submitted || porchd_form.porchd_description.$touched) }">
					<div class="col-lg-10">
						{{ Form::textarea('porchd_description',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.description'),
						'ng-model'=>'porchd_description','ng-minlength'=>"3",'ng-maxlength'=>"30"]) }}

						<p ng-show="porchd_form.porchd_description.$error.required && (porchd_form.$submitted || porchd_form.porchd_description.$touched)"
						 class="help-block"></p>
						<p ng-show="porchd_form.porchd_description.$error.minlength" class="help-block">Too short</p>
						<p ng-show="porchd_form.porchd_description.$error.maxlength" class="help-block">Too long</p>
					</div>
					<!--col-lg-10-->
				</div>
				<!--form control-->
			</div>
		</div>
	</div>
	<!--box-->

</form>