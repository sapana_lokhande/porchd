<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use App\Repositories\Backend\Package\PackageRepositoryContract;
use App\Repositories\Frontend\UserSubscription\UserSubscriptionRepository;

/**
 * Class UserController.
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
    */
    protected $users;

    /**
     * @var RoleRepository
    */
    protected $roles;

    /**
     * @var PackageRepositoryContract
    */
    protected $package;

    /**
     * @var PackageRepositoryContract
    */
    protected $user_subscription;


    /**
     * @param UserRepository $users
     * @param RoleRepository $roles
     */
    public function __construct(UserRepository $users, RoleRepository $roles, PackageRepositoryContract $package, UserSubscriptionRepository $user_subscription)
    {
        $this->users             = $users;
        $this->roles             = $roles;
        $this->package           = $package;
        $this->user_subscription = $user_subscription;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageUserRequest $request)
    {
        return view('backend.access.index');
    }

    /**
      * Get user types for porchd
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function list(ManageUserRequest $request)
    {
        $type  = $request->get('type') ?? null;
        $users = $this->users->getByRole([$type],'id')->pluck('name','id');
       
        return response()->json($users);
    }

    
    /**
     * Get user list for porchd
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function types(ManageUserRequest $request)
    {
        $roles= $this->roles->getByRole([1,2,3],'id')->pluck('name','id')->toArray();
        return response()->json($roles);
    }

    /**
     * Get user information for porchd
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function info(ManageUserRequest $request)
    {
        $user  = $request->get('user') ?? null;
        $data = $this->users->find($user);
        
        return response()->json($data);
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function create(ManageUserRequest $request)
    {   
        return view('backend.access.create')->withPackages($this->package->getPackage())
            ->withRoles($this->roles->getAll());
    }

    /**
     * @param StoreUserRequest $request
     *
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {
        $this->users->create(
            [
                'data'         => $request->only(
                                        'first_name',
                                        'last_name',
                                        'username',
                                        'state_assignment',
                                        'email',
                                        'password',
                                        'status',
                                        'confirmed',
                                        'confirmation_email'
                                    ),
                'roles'        => $request->only('assignees_roles'),
                'user_credits' => $request->only('user_credits')
            ]);

        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function show(User $user, ManageUserRequest $request)
    {
        return view('backend.access.show')
            ->withUser($user);
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request)
    {   
        $loginUserId = access()->user()->id;
        
        $package = $this->user_subscription->getUserAssigendPackageByAdmin($user->id,$loginUserId);
        
        return view('backend.access.edit')
            ->withPackages($this->package->getPackage())
            ->withPackage($package)
            ->withUser($user)
            ->withUserRoles($user->roles->pluck('id')->all())
            ->withRoles($this->roles->getAll());
    }

    /**
     * @param User              $user
     * @param UpdateUserRequest $request
     *
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->users->update($user,
            [
                'data'         => $request->only(
                                        'first_name',
                                        'last_name',
                                        'username',
                                        'state_assignment',
                                        'email',
                                        'status',
                                        'confirmed'
                                    ),
                'roles'        => $request->only('assignees_roles'),
                'user_credits' => $request->only('user_credits')
            ]);

        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function destroy(User $user, ManageUserRequest $request)
    {
        $this->users->delete($user);

        return redirect()->route('admin.access.user.deleted')->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }
}
