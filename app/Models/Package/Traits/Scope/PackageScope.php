<?php namespace App\Models\Package\Traits\Scope;

/**
 * Class PackageScope
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

trait PackageScope
{
    /**
     * @param $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('active', $status);
    }
}