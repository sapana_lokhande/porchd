@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.porchd.porchd-managment') . ' | ' . trans('labels.backend.porchd.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.porchd.upload') }}
        
    </h1>
@endsection

@section('content')
    <div ng-controller="PorchdController" data-ng-init="getPorchdDetail({{$porchd->id}})">
        @include('backend.porchd.form')
    </div>
@endsection

@section('after-scripts')
{!! Html::script('angular/porchd/controllers/porchd.js'); !!}
{!! Html::script('angular/porchd/services/porchd.js'); !!}
{!! Html::script('angular/user/services/user.js'); !!}
@endsection