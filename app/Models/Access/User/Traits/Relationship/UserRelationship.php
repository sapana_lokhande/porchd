<?php namespace App\Models\Access\User\Traits\Relationship;

/**
 * Class UserRelationship.
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 * @package App\Repositories\UserSubscription
 */

use App\Models\System\Session;
use App\Models\Access\User\SocialLogin;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }


    /**
     * @return mixed
     */
    public function subscription()
    {   
        return $this->hasMany('App\Models\UserSubscription\UserSubscription','user_id');
    }


    
    public function porchd()
    {
        return $this->hasMany('App\Models\Porchd\Porchd','user_id');
    }
}
