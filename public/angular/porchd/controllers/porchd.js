app.controller('PorchdController', ['$scope', 'Porchd', 'User', function ($scope, Porchd, User) {
	var vm = this;
	/* Attributes*/
	$scope.first_name = "";
	$scope.last_name = "";
	$scope.organisation_name = "";
	$scope.email = "";
	$scope.phone = "";
	$scope.address1 = "";
	$scope.address2 = "";
	$scope.state = "";
	//$scope.city = "";
	$scope.zip = "";
	$scope.update = false;
	$scope.porchdid = "";

	$scope.users_list = [];
	$scope.users_types = [];
	$scope.selectedUserTypes = '';
	$scope.selectedUserName = '';
	$scope.user_data = [];

	$scope.porchd_image_list = [];
	$scope.state_list = [];

	User.getUserTypes().then(function (response) {
		$scope.users_types = angular.fromJson(response.data);
	});


	Porchd.getStates().then(function (response) {
		$scope.state_list = response.data;
	});


	$scope.getUsers = function (type) {
		if (type) {


			User.getUsers(type).then(function (response) {
				$scope.users_list = angular.fromJson(response.data);

			});
		}
		else {
			$scope.resetData();
		}
	}


	$scope.getUserData = function (user) {
		if (user) {
			User.getUserData(user).then(function (response) {
				$scope.user_data = angular.fromJson(response.data);

				$scope.first_name = $scope.user_data.first_name;
				$scope.last_name = $scope.user_data.last_name;
				$scope.organisation_name = $scope.user_data.brokerage_name;
				$scope.email = $scope.user_data.email;
				$scope.phone = $scope.user_data.phone_number;
				$scope.address1 = $scope.user_data.address1;
				$scope.address2 = $scope.user_data.address2;
				$scope.state = $scope.user_data.state;
				$scope.city = '';
				$scope.zip = $scope.user_data.zip;
			});
		}
		else {
			$scope.resetData();
		}

	}

	$scope.resetData = function () {
		$scope.first_name = $scope.last_name = $scope.organisation_name
		$scope.email = $scope.phone = $scope.address1 = $scope.address2 = $scope.state = $scope.city = $scope.zip = '';
	}

	$scope.submitForm = function () {

		if ($scope.porchd_form.$valid) {
			if (!$scope.update) {
				Porchd.addPorchd($("#porchd_forms_step1").serialize()).then(function (response) {

				});

			}
			else {
				Porchd.updatePorchd($("#porchd_forms_step1").serialize(), $scope.porchdid).then(function (response) {

				});
			}


		}
	}

	$scope.getPorchdDetail = function (porchd) {
		$scope.update = true;
		Porchd.getPorchdDetail(porchd).then(function (response) {
			var data = response.data;
			$scope.getUserData(data.user_id);
			$scope.porchdid = data.id;
			$scope.porchd_address1 = data.address1;
			$scope.porchd_address2 = data.address2;
			$scope.porchd_state = data.state;
			$scope.porchd_city = data.city;
			$scope.porchd_zip = data.zip;
			$scope.porchd_label = data.name;
			$scope.porchd_sqfeet = data.square_feet;
			$scope.porchd_bedroom = data.bedrooms;
			$scope.porchd_bathroom = data.bathrooms;
			$scope.porchd_inquiry = data.property_for;
			$scope.porchd_description = data.description;
			$scope.getPorchdImageList(data.id);
		});


	}

	$scope.getPorchdImageList = function (porchd) {
		Porchd.getPorchdImageList(porchd).then(function (response) {
			$scope.porchd_image_list = response.data;
		});
	}

	$scope.saveImageDescription = function (x) {
		Porchd.savePorchdImageDescription(x).then(function (response) {
			console.log(response);
		});
	}

	$scope.claimPorchd = function (porchd) {
		Porchd.claimPorchd(porchd).then(function (response) {
			console.log(response);
		});

	}


}]);
