<?php

namespace App\Models\Porchd;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Porchd.
 */
class PorchdClaim extends Model
{

 
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['porchd_id','user_id','expiry_date'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.porchd_claim_table');
    }
}
