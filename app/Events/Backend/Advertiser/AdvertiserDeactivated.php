<?php namespace App\Events\Backend\Advertiser;

use Illuminate\Queue\SerializesModels;

/**
 * Class AdvertiserDeactivated.
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */
class AdvertiserDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $advertiser;

    /**
     * @param $advertiser
     */
    public function __construct($advertiser)
    {
        $this->advertiser = $advertiser;
    }
}
