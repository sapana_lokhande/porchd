//display roles list
app.factory('User', function($http){
	 var fac={};
	 
	 fac.getUserTypes=function(){
		  var _token=$('.token').val();
	  return  $http({
	       method: 'POST',
	       url: '/admin/access/user/types',
	       
	    });
	  }

		fac.getUsers=function(type){
		  var _token=$('.token').val();
	  return  $http({
	       method: 'POST',
	       url: '/admin/access/user/list',
	       data:{type:type}
	    });
		}
		

		fac.getUserData=function(user){
		  var _token=$('.token').val();
	  return  $http({
	       method: 'POST',
	       url: '/admin/access/user/info',
	       data:{user:user}
	    });
		}
		
		

  return fac;
});
