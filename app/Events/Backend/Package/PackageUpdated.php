<?php

namespace App\Events\Backend\Package;

use Illuminate\Queue\SerializesModels;

/**
 * Class PackageUpdated.
 */
class PackageUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $package;

    /**
     * @param $package
     */
    public function __construct($package)
    {
        $this->package = $package;
    }
}
