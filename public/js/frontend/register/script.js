$(function() {
    $('#real_estate_agent_div').css('display','none');

    // 31 Oct 2017 Admin can select only one role at a time
    //  Role is real estate agent or register user

     $('input.assignees_roles').on('change', function() {
        var value = $(this).val();
        $('input.assignees_roles').not(this).prop('checked', false); 
        if(value == 2)
        {
             $('#real_estate_agent_div').css('display','none');
        } 
        else 
        {
           $('#real_estate_agent_div').css('display','');
        } 
        
    });

    $('.date').datepicker({  

       format: 'mm-dd-yyyy'

     });  


});
