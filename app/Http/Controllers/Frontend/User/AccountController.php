<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\State\StateRepository;

/**
 * Class AccountController.
 */
class AccountController extends Controller
{


    /**
     * @var StateRepository
     */
    protected $state;


    /**
     * @param StateRepository $state
     */
    public function __construct(StateRepository $state)
    {
        
        $this->state = $state;
        
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	return view('frontend.user.account')->withState($this->state->getAll()->pluck('name','id'));
    }
}
