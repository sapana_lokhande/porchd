<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.advertiser.tabs.content.overview.name') }}</th>
        <td>{{ $advertiser->name }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.advertiser.tabs.content.overview.url') }}</th>
        <td>{{ $advertiser->url }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.advertiser.tabs.content.overview.status') }}</th>
        <td>{!! $advertiser->status_label !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.advertiser.tabs.content.overview.created_at') }}</th>
        <td>{{ $advertiser->created_at }} ({{ $advertiser->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.advertiser.tabs.content.overview.last_updated') }}</th>
        <td>{{ $advertiser->updated_at }} ({{ $advertiser->updated_at->diffForHumans() }})</td>
    </tr>

    @if ($advertiser->trashed())
        <tr>
            <th>{{ trans('labels.backend.advertiser.tabs.content.overview.deleted_at') }}</th>
            <td>{{ $advertiser->deleted_at }} ({{ $advertiser->deleted_at->diffForHumans() }})</td>
        </tr>
    @endif
</table>