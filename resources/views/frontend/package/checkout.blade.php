@extends('frontend.layouts.app')

@section('title', app_name() . ' | Purchase Package')

@section('content')
<div class="row">

    <div class="container wrapper">
        <p><a href="{{ url('package') }}">{{ trans('labels.frontend.package.purchase_package') }}</a> /<a href="{{ url('package/cart') }}"> Cart </a>/Checkout</p>
            <div class="row cart-head">
                <div class="container">
                <div class="row">
                    <p></p>
                </div>
                <div class="row">
                   Checkout
                </div>
                <div class="row">
                    <p>Please fill in the fields below to complete your purchase!</p>
                </div>
                </div>
            </div>    

            <div class="row cart-body">
              <!-- <form action="{{ url('package/payment') }}" method="POST" class="form-horizontal"> -->

         
              <!--  <form accept-charset="UTF-8" action="{{ url('package/payment') }}" class="require-validation"
                    data-cc-on-file="false"
                    data-stripe-publishable-key="pk_test_Aii1E9FtngXMUEOT9FTapHld"
                        id="payment-form" method="post"> -->
                <form action="{{ url('package/payment') }}" method="POST"  class="require-validation" id="payment-form">
               

                {!! csrf_field() !!}
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
                    <!--REVIEW ORDER-->
                    <div class="panel panel-info">
                        <div class="panel-heading">
                           3. Review Order
                        </div>
                        <div class="panel-body">
                            @if (sizeof($items) > 0)
            
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('labels.frontend.package.items') }}</th>
                                            <th>{{ trans('labels.frontend.package.price') }}</th>
                                            <th>{{ trans('labels.frontend.package.quantity') }}</th>
                                            <th>{{ trans('labels.frontend.package.total_price') }}</th>
                                            <th class="column-spacer"></th>
                                            
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($items as $item)
                                        <tr>
                                            <td>{{ $item->name }} - {{$item->options->listing_quantity}} property with {{$item->options->images_quantity}} images and {{$item->options->claim_quantity}} claims for {{$item->options->duration}} week</td>
                                            <td>${{ $item->price }}</td>
                                            <td>{{$item->qty}}</td>
                                            <td>${{ $item->subtotal }}</td>
                                            <td class=""></td>
                                            
                                        </tr>

                                        @endforeach
                                        <tr>
                                            <td class="table-image"></td>
                                            <td></td>
                                            <td class="small-caps table-bg" style="text-align: right">{{ trans('labels.frontend.package.subtotal') }}</td>
                                            <td>${{ Cart::instance('default')->subtotal() }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        
                                        <tr class="border-bottom">
                                            <td class="table-image"></td>
                                            <td style="padding: 40px;"></td>
                                            <td class="small-caps table-bg" style="text-align: right">{{ trans('labels.frontend.package.grand_total') }}</td>
                                            <td class="table-bg">${{ Cart::total() }}</td>
                                            <td class="column-spacer"></td>
                                            <td></td>
                                        </tr>

                                    </tbody>
                                </table>
                            @else

                                <h3>You have no items in your shopping cart</h3>
                                <a href="{{ url('package') }}" class="btn btn-primary btn-lg">{{  trans('buttons.frontend.package.continue_shopping') }}</a>

                            @endif
                           
                        </div>
                    </div>
                     <div class="panel panel-info">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary btn-submit-fix submit">Place Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--REVIEW ORDER END-->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6">
                    <!--SHIPPING METHOD-->
                    <div class="panel panel-info">
                        <div class="panel-heading">1. Billing Address</div>
                        <div class="panel-body">
                           
                            <div class="form-group">
                                <div class="col-md-6 col-xs-12">
                                    <strong>First Name:</strong>
                                    <input type="text" name="first_name" readonly class="form-control" value="{{$logged_in_user->first_name}}" />
                                </div>
                                <div class="span1"></div>
                                <div class="col-md-6 col-xs-12">
                                    <strong>Last Name:</strong>
                                    <input type="text" name="last_name" readonly class="form-control" value="{{$logged_in_user->first_name}}" />
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-xs-12">
                                    <strong>Email Address:</strong>
                                    <input type="text" name="email_address" readonly class="form-control" value="{{$logged_in_user->email}}" />
                                </div>
                                <div class="span1"></div>
                                <div class="col-md-6 col-xs-12">
                                    <strong>Telephone:</strong>
                                    <input type="text" name="phone_number" readonly class="form-control" value="{{$logged_in_user->phone_number}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12"><strong>Address:</strong></div>
                                <div class="col-md-12">
                                    <input type="text" name="address" readonly class="form-control" value="{{$logged_in_user->address1}}" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-6 col-xs-12">
                                    <strong>Country:</strong>
                                    <input type="text" name="country" readonly class="form-control" value="{{$logged_in_user->first_name}}" />
                                </div>
                                <div class="span1"></div>
                                <div class="col-md-6 col-xs-12">
                                    <strong>City:</strong>
                                     <input type="text" name="city" readonly class="form-control" value="{{$logged_in_user->city}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-xs-12">
                                    <strong>Zip Code:</strong>
                                    <input type="text" name="zip_code" readonly class="form-control" value="{{$logged_in_user->zip}}" />
                                </div>
                                <div class="span1"></div>
                                <div class="col-md-6 col-xs-12">
                                    <strong>State:</strong>
                                    <input type="text" name="state" readonly class="form-control" value="{{$logged_in_user->state}}" />
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--SHIPPING METHOD END-->
                    <!--CREDIT CART PAYMENT-->
                    <div class="panel panel-info">
                        <div class="panel-heading"><span><i class="glyphicon glyphicon-lock"></i></span> 2. Payment Method</div>
                         
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="payment-errors" style="color: red;"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12"><strong>Card Type:</strong></div>
                                <div class="col-md-12 required">
                                    <select id="CreditCardType" name="credit_card_type" class="form-control">
                                        <option value="Visa">Visa</option>
                                        <option value="MasterCard">MasterCard</option>
                                        <option value="American Express">American Express</option>
                                        <option value="Discover">Discover</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12"><strong>Name:</strong></div>
                                <div class="col-md-12 required"><input type="text" class="form-control required" placeholder="Name" name="name" value="" /></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12"><strong>Credit Card Number:</strong></div>
                                <div class="col-md-12 required"><input type="text" maxlength="16" data-stripe="number" class="form-control card-number" placeholder="Card Number" name="card_number" value="" /></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12"><strong>Card CVV:</strong></div>
                                <div class="col-md-12 required"><input maxlength="3" data-stripe="cvc" type="text" class="form-control card-cvc" name="card_code" placeholder="CVV" value="" /></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <strong>Expiration Date</strong>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 required">
                                  <input type="text" class="form-control card-expiry-month" maxlength="2" data-stripe="exp_month" name="month" value="" placeholder="Expiration Month" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 required">
                                    <input type="text" class="form-control card-expiry-year" maxlength="4" data-stripe="exp_year" name="year" value="" placeholder="Expiration Year" />
                                </div>
                            </div>
                            <input type="hidden" class="form-control total" name="amount" value="{{Cart::total()}}" />
                           
                        </div>
                    </div>
                    <!--CREDIT CART PAYMENT END-->
                    
                </div>
                </form>
            </div>
            <div class="row cart-footer">
        
            </div>
    </div>
    
    </div><!-- row -->
@endsection
@section('after-scripts')
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>
 <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
 <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
 <script type="text/javascript">
  Stripe.setPublishableKey('pk_test_Aii1E9FtngXMUEOT9FTapHld');
</script>
   <script>
        (function(){

             $('form.require-validation').bind('submit', function(e) {

                var $form         = $(e.target).closest('form'),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                                     'input[type=text]', 'input[type=file]',
                                     'textarea'].join(', '),
                    $inputs       = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid         = true;
                    $errorMessage.addClass('hide');

                $('.has-error').removeClass('has-error');
                $inputs.each(function(i, el) {
                  var $input = $(el);
                  if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault(); // cancel on first error
                  }
                });
              });


            $(function() {
                var $form = $('#payment-form');
                $form.submit(function(event) {
                    // Disable the submit button to prevent repeated clicks
                    $form.find('.submit').prop('disabled', true);

                     // Request a token from Stripe
                    Stripe.card.createToken($form, stripeResponseHandler);

                    // Prevent the form from being submitted:
                    return false;
                });
            });

             function stripeResponseHandler(status, response) {
                // Grab the form:
                var $form = $('#payment-form');

                if (response.error) { // Problem!
                    // Show the errors on the form:
                    $form.find('.payment-errors').text(response.error.message);
                    $form.find('.submit').prop('disabled', false); // Re-enable submission

                } else { // Token was created!

                    // Get the token ID:
                    var token = response.id;

                    // Insert the token ID into the form so it gets submitted to the server:
                    $form.append($('<input type="hidden" name="stripeToken">').val(token));

                    // Submit the form:
                    $form.get(0).submit();
                }
            };

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.quantity').on('change', function() {
                var id = $(this).attr('data-id')
                $.ajax({
                  type: "PATCH",
                  url: '{{ url("package/cart") }}' + '/' + id,
                  data: {
                    'quantity': this.value,
                  },
                  success: function(data) {
                    window.location.href = '{{ url('package/cart') }}';
                  }
                });

            });

        })();

    </script>
@endsection