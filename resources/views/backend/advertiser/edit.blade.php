@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.advertiser.management') . ' | ' . trans('labels.backend.advertiser.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.advertiser.management') }}
        <small>{{ trans('labels.backend.advertiser.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($advertiser, ['route' => ['admin.advertiser.update', $advertiser], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        @include('backend.advertiser.form')

    {{ Form::close() }}
@endsection