@extends('frontend.layouts.app')

@section('title', app_name() . ' | Purchase Package')

@section('content')
    <div class="row">
        <div class="container">
        <p><a href="{{ url('package') }}">{{ trans('labels.frontend.package.purchase_package') }}</a> / Cart</p>
        <h3>Your Cart</h3>

        <hr>

        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if (session()->has('error_message'))
            <div class="alert alert-danger">
                {{ session()->get('error_message') }}
            </div>
        @endif

        @if (sizeof(Cart::content()) > 0)

            <table class="table">
                <thead>
                    <tr>
                        <th>{{ trans('labels.frontend.package.shopping_cart') }}</th>
                        <th>{{ trans('labels.frontend.package.price') }}</th>
                        <th>{{ trans('labels.frontend.package.quantity') }}</th>
                        <th>{{ trans('labels.frontend.package.total_price') }}</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach (Cart::content() as $item)
                   
                    <tr>
                        <td>{{ $item->name }} - {{$item->options->listing_quantity}} property with {{$item->options->images_quantity}} images and {{$item->options->claim_quantity}} claims for {{$item->options->duration}} week</td>
                        <td>${{ $item->price }}</td>
                        <td>
                            <select class="quantity" data-id="{{ $item->rowId }}">
                                <option {{ $item->qty == 1 ? 'selected' : '' }}>1</option>
                                <option {{ $item->qty == 2 ? 'selected' : '' }}>2</option>
                                <option {{ $item->qty == 3 ? 'selected' : '' }}>3</option>
                                <option {{ $item->qty == 4 ? 'selected' : '' }}>4</option>
                                <option {{ $item->qty == 5 ? 'selected' : '' }}>5</option>
                            </select>
                        </td>
                        <td>${{ $item->subtotal }}</td>
                        <td class=""></td>
                        <td>
                            <form action="{{ url('package/cart', [$item->rowId]) }}" method="POST" class="side-by-side">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" class="btn btn-danger btn-sm" value="{{ trans('buttons.frontend.package.remove') }}">
                            </form>
                        </td>
                    </tr>

                    @endforeach
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">{{ trans('labels.frontend.package.subtotal') }}</td>
                        <td>${{ Cart::instance('default')->subtotal() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">{{ trans('labels.frontend.package.tax') }}</td>
                        <td>${{ Cart::instance('default')->tax() }}</td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">{{ trans('labels.frontend.package.total_price') }}</td>
                        <td class="table-bg">${{ Cart::total() }}</td>
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>

            <a href="{{ url('package') }}" class="btn btn-primary btn-lg">{{  trans('buttons.frontend.package.continue_shopping') }}</a> &nbsp;
            <form action="{{ url('package/checkout') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="submit" class="btn btn-success btn-lg" value="{{ trans('buttons.frontend.package.proceed_checkout') }}">
            </form>
           
            <div style="float:right">
                <form action="{{ url('package/emptyCart') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger btn-lg" value="{{ trans('buttons.frontend.package.empty_cart') }}">
                </form>
            </div>

        @else

            <h3>You have no items in your shopping cart</h3>
            <a href="{{ url('package') }}" class="btn btn-primary btn-lg">{{  trans('buttons.frontend.package.continue_shopping') }}</a>

        @endif

        <div class="spacer"></div>

    </div> <!-- end container -->
    </div><!-- row -->
@endsection
@section('after-scripts')
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>
 <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
   <script>
        (function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.quantity').on('change', function() {
                var id = $(this).attr('data-id')
                $.ajax({
                  type: "PATCH",
                  url: '{{ url("package/cart") }}' + '/' + id,
                  data: {
                    'quantity': this.value,
                  },
                  success: function(data) {
                    window.location.href = '{{ url('package/cart') }}';
                  }
                });

            });

        })();

    </script>
@endsection