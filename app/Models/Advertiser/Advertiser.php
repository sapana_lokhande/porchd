<?php namespace App\Models\Advertiser;

/**
 * Class Advertiser
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Advertiser\Traits\Scope\AdvertiserScope;
use App\Models\Advertiser\Traits\Attribute\AdvertiserAttribute;
use App\Models\Advertiser\Traits\Relationship\AdvertiserRelationship;

/**
 * Class Advertiser.
 */
class Advertiser extends Model
{
    use AdvertiserScope,
        SoftDeletes,
        AdvertiserAttribute,
        AdvertiserRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 
        'name', 
        'url', 
        'active'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->table = config('backend.advertisers_table');
    }
}
