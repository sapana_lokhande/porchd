<?php


Route::group(['namespace' => 'Porchd'], function () {

        /*
         * Porchd Status'
         */
        Route::get('porchd/deactivated', 'PorchdController@getDeactivated')->name('porchd.deactivated');
        Route::get('porchd/deleted', 'PorchdController@getDeleted')->name('porchd.deleted');
        Route::post('porchd/get', 'PorchdController')->name('porchd.get');
        /*
         * Porchd CRUD
         */
        Route::resource('porchd', 'PorchdController');
    
        Route::post('porchd/info', 'PorchdController@getInfo');
        Route::post('porchd/getstates', 'PorchdController@getstates');
        Route::any('porchd/claimporchd', 'PorchdController@claimPorchd');

        Route::post('porchd/addimage/{id}', 'PorchdController@addPorchdImage')->name('porchd.addimage');
        Route::post('porchd/imagelist', 'PorchdController@imageList');

        Route::post('porchd/saveimageinfo', 'PorchdController@saveImageInfo');
        
        // Active
        Route::get('porchd/mark/{id}/{status}', 'PorchdController@mark')->name('porchd.mark')->where(['active' => '[0,1]']); 
    
        /*
         * Deleted Porchd
         */
        Route::get('porchd/delete/{id}', 'PorchdController@delete')->name('porchd.delete-permanently');
        Route::get('porchd/restore/{id}', 'PorchdController@restore')->name('porchd.restore');
});
