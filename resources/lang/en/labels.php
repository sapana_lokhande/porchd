<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => 'All',
        'yes'     => 'Yes',
        'no'      => 'No',
        'custom'  => 'Custom',
        'actions' => 'Actions',
        'active'  => 'Active',
        'buttons' => [
            'save'   => 'Save',
            'update' => 'Update',
        ],
        'hide'              => 'Hide',
        'inactive'          => 'Inactive',
        'none'              => 'None',
        'show'              => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions'     => 'Permissions',
                    'role'            => 'Role',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                ],
            ],

            'users' => [
                'active'              => 'Active Users',
                'all_permissions'     => 'All Permissions',
                'change_password'     => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create'              => 'Create User',
                'deactivated'         => 'Deactivated Users',
                'deleted'             => 'Deleted Users',
                'edit'                => 'Edit User',
                'management'          => 'User Management',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',

                'table' => [
                    'confirmed'        => 'Confirmed',
                    'created'          => 'Created',
                    'email'            => 'E-mail',
                    'id'               => 'ID',
                    'last_updated'     => 'Last Updated',
                    'name'             => 'Name',
                    'first_name'       => 'First Name',
                    'last_name'        => 'Last Name',
                    // 27 oct 2017 added new fields in user mgnt section start
                    'username'         => 'Username',
                    'state_assignment' => 'State Assignment',
                    'user_credits'     => 'User Credits',
                    // 27 oct 2017 added new fields in user mgnt section end
                    'no_deactivated'   => 'No Deactivated Users',
                    'no_deleted'       => 'No Deleted Users',
                    'roles'            => 'Roles',
                    'social'           => 'Social',
                    'total'            => 'user total|users total',
                    'brokarage_name'    => 'Organization Name'
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmed',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'email'        => 'E-mail',
                            'last_updated' => 'Last Updated',
                            'name'         => 'Name',
                            'first_name'   => 'First Name',
                            'last_name'    => 'Last Name',
                            'status'       => 'Status',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
            
            'permissions' => [
                'create'     => 'Create Permission',
                'edit'       => 'Edit Permission',
                'management' => 'Permission Management',

                'table' => [
                    'permissions'     => 'Permissions',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                    'name'            => 'Name',
                    'disp_name'       => 'Display Name'
                ],
            ],
            
            ],
            
            'package' => [
                'create'     => 'Create Package',
                'edit'       => 'Edit Permission',
                'management' => 'Package Management',
                'active'     =>  'Active Packages',  
                'deactivated'         => 'Deactivated Packages',
                'deleted'             => 'Deleted Packages',
                'view' => 'View Package',
                'table' => [
                    'id'               => 'ID',
                    'last_updated'     => 'Last Updated',
                    'name'             => 'Name',
                    'price'            => 'Price',
                    'created_at'       => 'Created At',
                ],
                 'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'description'        => 'Description',
                            'user_type'          => 'Confirmed',
                            'created_at'         => 'Created At',
                            'deleted_at'         => 'Deleted At',
                            'price'              => 'Price',
                            'last_updated'       => 'Last Updated',
                            'name'               => 'Name',
                            'listing_quantity'   => 'Listing Quantity',
                            'images_quantity'    => 'Images Quantity',
                            'status'             => 'Status',
                            'claim_quantity'     => 'Claim Quantity'
                        ],
                    ],
                ],
        ],
        'advertiser' => [ 
            'active'      => 'Active Advertiser',
            'create'      => 'Create Advertiser',
            'deactivated' => 'Deactivated Advertiser',
            'deleted'     => 'Deleted Advertiser',
            'edit'        => 'Edit Advertiser',
            'management'  => 'Advertiser Management',

            'table' => [
                'created'        => 'Created',
                'id'             => 'ID',
                'last_updated'   => 'Last Updated',
                'name'           => 'Name',
                'url'            => 'URL',
                'no_deactivated' => 'No Deactivated Advertisers',
                'no_deleted'     => 'No Deleted Advertisers',
                'total'          => 'advertiser total|advertisers total',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],

                'content' => [
                    'overview' => [
                        'created_at'    => 'Created At',
                        'deleted_at'    => 'Deleted At',
                        'url'           => 'URL',
                        'last_updated'  => 'Last Updated',
                        'name'          => 'Name',
                        'status'        => 'Status',
                    ],
                ],
            ],

            'view' => 'View Advertiser',
        ],
        'porchd' => [
            'porchd-managment' => 'Porchd Managment',
            'create'           => 'Create Porchd',
            'upload'           => 'Upload Porchd',
            'form'             => [
                                    'step1' =>'Step 1 - Information',
                                    'step2' =>'Step 2 - Upload Photos',
                                    'step3' =>'Step 3 - Photo Descriptions',
                                    'step4' =>'Step 4 - Cliam Listing',
  
            ],
            'table'    =>[
                'id'    =>  'Id',
                'label'    =>  'Label',
                'description'   =>'Description',
                'state'    =>  'State',
                'city' =>'City',
                'bedrooms' =>  'Bedrooms',
                'bathrooms'    =>  'Bathrooms',
                'country'  =>  'Country',
                'zip'  =>  'Zip',
                'active'   =>  'Active',
                'property_for' =>'Porchd Type'
            ],
            'active_porchd' =>  'Active Porchd',
            'porchd_managment'  =>  'Porchd Managmemt'
        ]
    ],

    'frontend' => [

        'package' => [
            'purchase_package' => 'Purchase Package',
            'shopping_cart' => 'Shopping Cart',
            'price' => 'Price',
            'quantity' => 'Quantity',
            'total_price' => 'Total Price',
            'your_total' => 'Your Total',
            'tax' => 'Tax',
            'subtotal' => 'Subtotal',
            'grand_total' => 'Grand Total',
            'items' => 'Items'
        ],

        'auth' => [
            'login_box_title'    => 'Login',
            'login_button'       => 'Login',
            'login_with'         => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button'    => 'Register',
            'remember_me'        => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Send Information',
        ],

        'passwords' => [
            'forgot_password'                 => 'Forgot Your Password?',
            'reset_password_box_title'        => 'Reset Password',
            'reset_password_button'           => 'Reset Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'macros' => [
            'country' => [
                'alpha'   => 'Country Alpha Codes',
                'alpha2'  => 'Country Alpha 2 Codes',
                'alpha3'  => 'Country Alpha 3 Codes',
                'numeric' => 'Country Numeric Codes',
            ],

            'macro_examples' => 'Macro Examples',

            'state' => [
                'mexico' => 'Mexico State List',
                'us'     => [
                    'us'       => 'US States',
                    'outlying' => 'US Outlying Territories',
                    'armed'    => 'US Armed Forces',
                ],
            ],

            'territories' => [
                'canada' => 'Canada Province & Territories List',
            ],

            'timezone' => 'Timezone',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar'             => 'Avatar',
                'created_at'         => 'Created At',
                'edit_information'   => 'Edit Information',
                'email'              => 'E-mail',
                'last_updated'       => 'Last Updated',
                'name'               => 'Name',
                'first_name'         => 'First Name',
                'last_name'          => 'Last Name',
                'update_information' => 'Update Information',
                'role'               => 'Role',
                'birthdate'          => 'Birthdate',
                'license_number'     => 'License Number',
                'state_assignment'   => 'State Assignment',
            ],
        ],

    ],
];
