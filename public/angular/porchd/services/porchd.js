
app.factory('Porchd', function ($http) {
	var fac = {};
	fac.addPorchd = function (data) {
		return $http({
			method: 'POST',
			url: '/admin/porchd',
			dataType : 'json',
			data:{data:data}
		})
	}

	fac.updatePorchd = function (data,porchdid) {
		return $http({
			method: 'PATCH',
			url: '/admin/porchd/'+porchdid,
			dataType : 'json',
			data:{data:data}
		})
	}

	fac.getPorchdDetail = function (data) {
		return $http({
			method: 'POST',
			url: '/admin/porchd/info',
			data:{data:data}
		})
	}
	
	fac.getPorchdImageList = function (data) {
		return $http({
			method: 'POST',
			url: '/admin/porchd/imagelist',
			data:{data:data}
		})
	}

	fac.savePorchdImageDescription = function (data) {
		return $http({
			method: 'POST',
			url: '/admin/porchd/saveimageinfo',
			data:{data:data}
		})
	}

	fac.getStates = function (data) {
		return $http({
			method: 'POST',
			url: '/admin/porchd/getstates',
			data:{data:data}
		})
	}

	fac.claimPorchd = function (data) {
		return $http({
			method: 'POST',
			url: '/admin/porchd/claimporchd',
			data:{data:data}
		})
	}
	
	
	return fac;
});
