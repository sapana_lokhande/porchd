<?php

return [

'property_for'  => [
    0           => 'Sell',
    1           => 'View'
],

'images'        => [
    'thumbnail' => 'Thumbnail',
    'large'     => 'Large',
    'medium'    => 'Medium'
]
];
