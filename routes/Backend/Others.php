<?php

/**
* Advertiser
*/
Route::group(['namespace' => 'Advertiser'], function () {
    /*
     * For DataTables
     */
    Route::post('advertiser/get', 'AdvertiserController')->name('advertiser.get');

    /*
     * Advertiser Status'
     */
    Route::get('advertiser/deactivated', 'AdvertiserController@getDeactivated')->name('advertiser.deactivated');
    Route::get('advertiser/deleted', 'AdvertiserController@getDeleted')->name('advertiser.deleted');

    /*
     * Advertiser CRUD
     */
    Route::resource('advertiser', 'AdvertiserController');

    // Active
    Route::get('advertiser/mark/{id}/{status}', 'AdvertiserController@mark')->name('advertiser.mark')->where(['active' => '[0,1]']); 

    /*
     * Deleted Advertiser
     */
    Route::get('advertiser/delete/{id}', 'AdvertiserController@delete')->name('advertiser.delete-permanently');
    Route::get('advertiser/restore/{id}', 'AdvertiserController@restore')->name('advertiser.restore');

});

