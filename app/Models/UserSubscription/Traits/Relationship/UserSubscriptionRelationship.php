<?php namespace App\Models\UserSubscription\Traits\Relationship;

/**
 * Class UserSubscriptionRelationship
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

trait UserSubscriptionRelationship
{

	/**
     * @return mixed
     */

	public function users()
    {
    	 return $this->belongsTo('App\Models\Access\User\User','user_id','id');
    }

}
