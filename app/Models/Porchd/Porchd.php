<?php

namespace App\Models\Porchd;

use Illuminate\Database\Eloquent\Model;
use App\Models\Porchd\Traits\PorchdAccess;
use App\Models\Porchd\Traits\Scope\PorchdScope;
use App\Models\Porchd\Traits\Attribute\PorchdAttribute;
use App\Models\Porchd\Traits\Relationship\PorchdRelationship;

/**
 * Class Porchd.
 */
class Porchd extends Model
{
    use PorchdScope,
        PorchdAccess,
        PorchdAttribute,
        PorchdRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','name','short_description','long_description','property_for','square_feet','bedrooms','bathrooms','description','country','state','city','zip','latitude','longitude','active','published_date','expiry_date','address1','address2'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.porchd_table');
    }
}
