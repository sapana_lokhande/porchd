@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.advertiser.management') . ' | ' . trans('labels.backend.advertiser.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.advertiser.management') }}
        <small>{{ trans('labels.backend.advertiser.create') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.advertiser.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

    @include('backend.advertiser.form')

    {{ Form::close() }}
@endsection
