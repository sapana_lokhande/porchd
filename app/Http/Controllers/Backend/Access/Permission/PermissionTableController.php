<?php

namespace App\Http\Controllers\Backend\Access\Permission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Access\Permission\PermissionRepository;
use App\Http\Requests\Backend\Access\Permission\ManagePermission;
class PermissionTableController extends Controller
{
   /**
     * @var PermissionRepository
     */
    protected $permission;

    /**
     * @param PermissionRepository $permission
     */
    public function __construct(PermissionRepository $permission)
    {
        $this->permission = $permission;
    }

    /**
     * @param ManagePermission $request
     *
     * @return mixed
     */
    public function __invoke(ManagePermission $request)
    {

    	return Datatables::of($this->permission->getForDataTable())
            ->escapeColumns(['name', 'sort'])
            ->addColumn('name', function ($permission) {
                return $permission->count() ?
                   $permission->name:
                    '<span class="label label-danger">'.trans('labels.general.none').'</span>';
            })
            ->addColumn('display_name', function ($permission) {
                return $permission->display_name;
            })
 			
 			->addColumn('sort', function ($permission) {
                return $permission->sort;
            })
            ->addColumn('actions', function ($permission) {
                return $permission->action_buttons;
            })
            ->make(true);
    }
}
