<?php namespace App\Events\Backend\Advertiser;

use Illuminate\Queue\SerializesModels;

/**
 * Class AdvertiserUpdated.
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */
class AdvertiserUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $advertiser;

    /**
     * @param $advertiser
     */
    public function __construct($advertiser)
    {
        $this->advertiser = $advertiser;
    }
}
