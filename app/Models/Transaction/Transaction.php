<?php namespace App\Models\Transaction;

/**
 * Class Transaction
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Transaction\Traits\Relationship\TransactionRelationship;

class Transaction extends Model
{

    use SoftDeletes,
        TransactionRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'transaction_id','user_id','request','response'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = [];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {   
        parent::__construct($attributes);
     
        $this->table = config('backend.transactions_table');
    }

}
