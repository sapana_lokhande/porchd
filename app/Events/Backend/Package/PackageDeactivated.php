<?php namespace App\Events\Backend\Package;

use Illuminate\Queue\SerializesModels;

/**
 * Class PackageDeactivated.
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */
class PackageDeactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $package;

    /**
     * @param $package
     */
    public function __construct($package)
    {
        $this->package = $package;
    }
}
