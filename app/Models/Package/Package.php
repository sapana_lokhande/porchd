<?php namespace App\Models\Package;

/**
 * Class Package
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Package\Traits\Scope\PackageScope;
use App\Models\Package\Traits\Attribute\PackageAttribute;
use App\Models\Package\Traits\Relationship\PackageRelationship;

class Package extends Model
{

    use PackageScope,
        SoftDeletes,
        PackageAttribute,
        PackageRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','description','user_type','listing_quantity','images_quantity','claim_quantity','price','active'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = [];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {   
        parent::__construct($attributes);
     
        $this->table = config('backend.subscriptions_table');
    }

}
