@extends('frontend.layouts.app')

@section('title', app_name() . ' | Purchase Package')

@section('content')
    <div class="row">
      @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
      @endif
      @foreach ($packages as $key => $value) 
            <div class="col-md-3">
                    <div class="thumbnail">
                        <div class="caption text-center">
                            
                             <h2>{{$value->name}}</h2>
                              <p>{{$value->listing_quantity}} Property</p>
                              <p>{{$value->images_quantity}} Images each</p>
                              <p>{{$value->claim_quantity}} Week</p>
                              <p>Price : $ {{$value->price}} </p>
                               <form action="{{ url('package/cart') }}" method="POST" class="side-by-side">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{ $value->id }}">
                                <input type="hidden" name="name" value="{{ $value->name }}">
                                <input type="hidden" name="price" value="{{ $value->price }}">
                                <input type="hidden" name="listing_quantity" value="{{ $value->listing_quantity }}">
                                <input type="hidden" name="images_quantity" value="{{ $value->images_quantity }}">
                                <input type="hidden" name="claim_quantity" value="{{ $value->claim_quantity }}">
                                <input type="submit" class="btn btn-success btn-lg" value="{{  trans('buttons.frontend.package.add_to_cart') }}">
                            </form>
                        </div> <!-- end caption -->

                    </div> <!-- end thumbnail -->
                </div> <!-- end col-md-3 -->
      @endforeach
    </div><!-- row -->
@endsection