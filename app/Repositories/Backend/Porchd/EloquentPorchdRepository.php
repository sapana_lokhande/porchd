<?php namespace App\Repositories\Backend\Porchd;

/**
 * Class EloquentPorchdRepository
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 * @package App\Repositories\Backend\Porchd
 */

use App\Models\Porchd\Porchd;
use App\Models\Porchd\PorchdImage;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

// use App\Events\Backend\Porchd\PorchdCreated;
// use App\Events\Backend\Porchd\PorchdDeleted;
// use App\Events\Backend\Porchd\PorchdUpdated;
// use App\Events\Backend\Porchd\PorchdRestored;
// use App\Events\Backend\Porchd\PorchdDeactivated;
// use App\Events\Backend\Porchd\PorchdReactivated;
// use App\Events\Backend\Porchd\PorchdPermanentlyDeleted;
use App\Repositories\Backend\Porchd\PorchdRepositoryContract;
use Image;
use File;
use App\Models\State\State;
use App\Models\Porchd\PorchdClaim;

/**
 * Class EloquentPorchdRepository.
 */
class EloquentPorchdRepository extends BaseRepository implements PorchdRepositoryContract
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Porchd::class;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Porchd Image Model
     */
    protected $porchd_image;

    /**
     * Construct
     *
     */
    public function __construct()
    {
        $this->model = new Porchd();
        $this->porchd_image = new PorchdImage();
    }
    

    public function getState()
    {
        $states= State::all()->pluck('name','id');
        return $states;
    }
    /**
     * Get For Data Table
     *
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            ->with('user')
           
            ->select([
                config('backend.access.porchd_table').'.id',
                config('backend.access.porchd_table').'.description',
                config('backend.access.porchd_table').'.name',
                config('backend.access.porchd_table').'.user_id',
                config('backend.access.porchd_table').'.state',
                config('backend.access.porchd_table').'.city',
                
            ]);
        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    /**
     * Create
     *
     * @param array $input
     */
    public function create($input)
    {
        $data       = $input['data'][0];
        $porchd = $this->createPorchdStub($data);
        DB::transaction(function () use ($porchd, $data) {
            if ($porchd->save()) {
               // event(new PorchdCreated($porchd));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.advertiser.create_error'));
        });
    }

    /**
     * Update
     *
     * @param Model $porchd
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $porchd, array $input)
    {
        $data = $input;
        $this->updatePorchdStub($data, $porchd);
        DB::transaction(function () use ($porchd, $data) {
            if ($porchd->save()) {
                //event(new PorchdUpdated($porchd));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.advertiser.update_error'));
        });
    }

    /**
     * Delete
     *
     * @param Model $porchd
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Model $porchd)
    {
        if ($porchd->delete()) {
            event(new PorchdDeleted($porchd));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.advertiser.delete_error'));
    }

    /**
     * Force Delete
     *
     * @param Model $porchd
     *
     * @throws GeneralException
     */
    public function forceDelete(Model $porchd)
    {
        if (is_null($porchd->deleted_at)) {
            throw new GeneralException(trans('exceptions.backend.advertiser.delete_first'));
        }

        DB::transaction(function () use ($porchd) {
            if ($porchd->forceDelete()) {
                event(new PorchdPermanentlyDeleted($porchd));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.advertiser.delete_error'));
        });
    }

    /**
     * Restore
     *
     * @param Model $porchd
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function restore(Model $porchd)
    {
        if (is_null($porchd->deleted_at)) {
            throw new GeneralException(trans('exceptions.backend.advertiser.cant_restore'));
        }

        if ($porchd->restore()) {
            event(new PorchdRestored($porchd));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.advertiser.restore_error'));
    }

    /**
     * Mark
     *
     * @param Model $porchd
     * @param $status
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function mark(Model $porchd, $status)
    {
        $porchd->status = $status;

        switch ($status) {
            case 0:
                event(new PorchdDeactivated($porchd));
                break;

            case 1:
                event(new PorchdReactivated($porchd));
                break;
        }

        if ($porchd->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.advertiser.mark_error'));
    }

    /**
     * Check Porchd By Name
     *
     * @param  $input
     * @param  $porchd
     *
     * @throws GeneralException
     */
    protected function checkPorchdByName($input, $porchd)
    {
        //Figure out if name is not the same
        if ($porchd->name != $input['name']) {
            //Check to see if name exists
            if ($this->query()->where('name', '=', $input['name'])->first()) {
                throw new GeneralException(trans('exceptions.backend.advertiser.name'));
            }
        }
    }

    /**
     * Create Porchd Stub
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    protected function createPorchdStub($input)
    {

        $this->model->user_id      = $input['selectedUserName'];
        $this->model->state        = $input['porchd_state'];
        $this->model->city         = $input['porchd_city'];
        $this->model->zip          = $input['porchd_zip'];
        $this->model->description  = $input['porchd_description'];
        $this->model->property_for = $input['porchd_inquiry']?? 0;
        $this->model->square_feet  = $input['porchd_sqfeet'] ;
        $this->model->address1          = $input['porchd_address1'] ;
        $this->model->address2          = $input['porchd_address2'] ;
        $this->model->active       = 1;
        $this->model->bedrooms     = $input['porchd_bedroom'];
        $this->model->bathrooms    = $input['porchd_bathroom'];
        $this->model->name         = $input['porchd_label'];
        
        return $this->model;
    }

        /**
     * Create Porchd Stub
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    protected function updatePorchdStub($input, $porchd)
    {

        //$porchd->user_id      = $input['selectedUserName'];
        $porchd->state        = $input['porchd_state'];
        $porchd->city         = $input['porchd_city'];
        $porchd->zip          = $input['porchd_zip'];
        $porchd->description  = $input['porchd_description'];
        $porchd->property_for = $input['porchd_inquiry']?? 0;
        $porchd->square_feet  = $input['porchd_sqfeet'] ;
        $porchd->address1     = $input['porchd_address1'] ;
        $porchd->address2     = $input['porchd_address2'] ;
        $porchd->active       = 1;
        $porchd->bedrooms     = $input['porchd_bedroom'];
        $porchd->bathrooms    = $input['porchd_bathroom'];
        $porchd->name         = $input['porchd_label'];
        
        return $this->model;
    }

    /**
     * addPorhdImage function
     *
     * @return void
     */
    public function addPorhdImage($image, $porchd)
    {
        $imageName = time().$image->getClientOriginalName();
        $this->saveLarge($image, $porchd, $imageName);
        $this->saveThumbnail($image, $porchd, $imageName);
        $this->saveMedium($image, $porchd, $imageName);
        $this->saveImageToDB($porchd,$imageName);
        
        return ['success'=>$imageName];
    }

    /**
     * saveThumbline function
     *
     * @param File $image
     * @param Int $porchd
     * @return void
     */
    protected function saveThumbnail($image, $porchd, $imageName)
    {
        $thumbnailPath = public_path('images') . "/" . $porchd ."/" . config('porchd.images.thumbnail');
        if (!File::isDirectory($thumbnailPath)) {
            File::makeDirectory($thumbnailPath, 0777, true, true);
        }
        Image::make($image->getRealPath(), [
            'width' => 100,
            'height' => 100,
            'grayscale' => false])->save($thumbnailPath.'/'.$imageName);
        
        //$this->saveImageToDB('Thumbnail', $porchd, $thumbnailPath, $imageName);
    }
       /**
     * saveMedium function
     *
     * @param File $image
     * @param Int $porchd
     * @return void
     */
    protected function saveMedium($image, $porchd, $imageName)
    {
        $mediumPath=     public_path('images') . "/" . $porchd ."/" . config('porchd.images.medium');
        if (!File::isDirectory($mediumPath)) {
            File::makeDirectory($mediumPath, 0777, true, true);
        }
        Image::make($image->getRealPath(), [
            'width' => 200,
            'height' => 200,
            'grayscale' => false])->save($mediumPath.'/'.$imageName);
        
        //$this->saveImageToDB('Medium', $porchd, $mediumPath, $imageName);
    }

           /**
     * saveLarge function
     *
     * @param File $image
     * @param Int $porchd
     * @return void
     */
    protected function saveLarge($image, $porchd, $imageName)
    {
        $largePath =  public_path('images') . "/" . $porchd ."/" . config('porchd.images.large');
        
        if (!File::isDirectory($largePath)) {
            File::makeDirectory($largePath, 0777, true, true);
        }
        Image::make($image->getRealPath(), [
            'width' => 500,
            'height' => 500,
            'grayscale' => false])->save($largePath.'/'.$imageName);

        //$this->saveImageToDB('Large', $porchd, $largePath, $imageName);
    }

    // protected function saveImageToDB($type, $id, $path = '', $imageName = '')
    // {
    //     $porchd_image            = new PorchdImage();
    //     $porchd_image->porchd_id = $id;
    //     $porchd_image->path      = $path;
    //     $porchd_image->name      = $imageName;
    //     $porchd_image->active    = 1;
    //     $porchd_image->type    = $type;
    //     $porchd_image->save();

    // }
    protected function saveImageToDB($id, $imageName)
    {
        $porchd_image            = new PorchdImage();
        $porchd_image->porchd_id = $id;
        $porchd_image->name      = $imageName;
        $porchd_image->active    = 1;
        $porchd_image->save();

    }
  
    public function saveImageInfo($input)
    {
        $porchd_image = $this->porchd_image->find($input['id']);
        $porchd_image->long_description = $input['long_description'];
        return $porchd_image->save();

    }

    public function claimPorchd($input)
    {
        $user=  access()->user();
        $porchd=    $this->model->with('porchdClaim')->find($input);

        if(empty($porchd->PorchdClaim->user_id))
        {
            $porchd_claim            = new PorchdClaim();
            $porchd_claim->user_id   = $user->id;
            $porchd_claim->porchd_id = $porchd->id;
            $porchd_claim->save();
        }
        return ['success'=>true];
    }
}
