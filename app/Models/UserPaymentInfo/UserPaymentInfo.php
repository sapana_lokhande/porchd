<?php namespace App\Models\UserPaymentInfo;

/**
 * Class UserPaymentInfo
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UserPaymentInfo\Traits\Relationship\UserPaymentInfoRelationship;

class UserPaymentInfo extends Model
{

    use SoftDeletes,
        UserPaymentInfoRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'user_payment_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id','customer_profile_id','name','credit_card','exp_month','exp_year','card_type'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = [];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {   
        parent::__construct($attributes);
     
        $this->table = config('backend.user_payment_info_table');
    }

}
