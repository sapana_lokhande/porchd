<?php namespace App\Http\Controllers\Backend\Package;

/**
 * Class PackageController
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Http\Request;
use App\Models\Package\Package;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Package\StoreRequest;
use App\Http\Requests\Backend\Package\ManageRequest;
use App\Http\Requests\Backend\Package\UpdateRequest;
use App\Repositories\Backend\Package\PackageRepositoryContract;

class PackageController extends Controller
{
    /**
     * PackageRepositoryContract
     *
     * @var object
     */
    protected $repository;


    /**
     * Construct
     *
     * @param PackageRepositoryContract
     */
    public function __construct(PackageRepositoryContract $repository)
    {
          $this->repository = new $repository;
    }

    /**
     * Invoke
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
     public function __invoke(ManageRequest $request)
    {

        return Datatables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->escapeColumns(['name'])
            ->addColumn('actions', function ($package) {
                return $package->action_buttons;
            })
            ->withTrashed()
            ->make(true);
    }

    /**
     * Index 
     *
     * @param ManageRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageRequest $request)
    {
        return view('backend.package.index');
    }

     /**
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function create(ManageRequest $request)
    {
        return view('backend.package.create');
    }

    /**
     * @param StoreRequest $request
     *
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        $this->repository->create(
        [
            'data' => $request->only(
                'name',
                'description',
                'user_type',
                'listing_quantity',
                'images_quantity',
                'claim_quantity',
                'price',
                'active'
            )
        ]);

        return redirect()->route('admin.package.index')->withFlashSuccess(trans('alerts.backend.package.created'));
    }

   
    /**
     * Show 
     *
     * @param Package    $package
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function show(Package $package, ManageRequest $request)
    {
        return view('backend.package.show')
            ->withPackage($package);
    }

    /**
     * Edit 
     *
     * @param Package    $package
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function edit(Package $package, ManageRequest $request)
    {
        return view('backend.package.edit')
            ->withPackage($package);
    }

    /**
     * Update 
     *
     * @param Package    $package
     * @param UpdateRequest $request
     *
     * @return mixed
     */
    public function update(Package $package, UpdateRequest $request)
    {
        $this->repository->update($package,
            [
                'data' => $request->only(
                    'name',
                    'description',
                    'user_type',
                    'listing_quantity',
                    'images_quantity',
                    'claim_quantity',
                    'price',
                    'active'
                )
            ]);

        return redirect()->route('admin.package.index')->withFlashSuccess(trans('alerts.backend.package.updated'));
    }

    /**
     * Destory 
     *
     * @param Package    $package
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function destroy(Package $package, ManageRequest $request)
    {
        $this->repository->delete($package);

        return redirect()->route('admin.package.deleted')->withFlashSuccess(trans('alerts.backend.package.deleted'));
    }

    /**
     * Get Deactivated 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function getDeactivated(ManageRequest $request)
    {
        return view('backend.package.deactivated');
    }

    /**
     * Get Deleted 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function getDeleted(ManageRequest $request)
    {
        return view('backend.package.deleted');
    }

    /**
     * Mark
     *
     * @param $id
     * @param $status
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function mark($id, $status, ManageRequest $request)
    {
        $package = $this->repository->findOrThrowException($id);

        $this->repository->mark($package, $status);

        return redirect()->route($status == 1 ? 'admin.package.index' : 'admin.package.deactivated')->withFlashSuccess(trans('alerts.backend.package.updated'));
    }

    /**
     * Delete
     *
     * @param $id
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function delete($id, ManageRequest $request)
    {
        $package = $this->repository->findOrThrowException($id);

        $this->repository->forceDelete($package);

        return redirect()->route('admin.package.deleted')->withFlashSuccess(trans('alerts.backend.package.deleted_permanently'));
    }

    /**
     * Resote
     *
     * @param $id
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function restore($id, ManageRequest $request)
    {
        $package = $this->repository->findOrThrowException($id);

        $this->repository->restore($package);

        return redirect()->route('admin.package.index')->withFlashSuccess(trans('alerts.backend.package.restored'));
    }
}