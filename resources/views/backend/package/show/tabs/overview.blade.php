<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.name') }}</th>
        <td>{{ $package->name }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.description') }}</th>
        <td>{{ $package->description }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.user_type') }}</th>
        <td>{{ $package->user_type }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.listing_quantity') }}</th>
        <td>{{ $package->listing_quantity }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.images_quantity') }}</th>
        <td>{{ $package->images_quantity }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.claim_quantity') }}</th>
        <td>{{ $package->claim_quantity }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.price') }}</th>
        <td>{{ $package->price }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.status') }}</th>
        <td>{!! $package->status_label !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.created_at') }}</th>
        <td>{{ $package->created_at }} ({{ $package->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.package.tabs.content.overview.last_updated') }}</th>
        <td>{{ $package->updated_at }} ({{ $package->updated_at->diffForHumans() }})</td>
    </tr>

    @if ($package->trashed())
        <tr>
            <th>{{ trans('labels.backend.package.tabs.content.overview.deleted_at') }}</th>
            <td>{{ $package->deleted_at }} ({{ $package->deleted_at->diffForHumans() }})</td>
        </tr>
    @endif
</table>

