<?php namespace App\Http\Requests\Backend\Porchd;

/**
 * Class StoreRequest
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         
        ];
    }
}
