<?php

namespace App\Http\Controllers\Backend\Porchd;

use App\Models\Access\User\User;
use App\Models\Porchd\Porchd;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Porchd\StoreRequest;
use App\Http\Requests\Backend\Porchd\UpdateRequest;
use App\Http\Requests\Backend\Porchd\ManageRequest;
use App\Repositories\Backend\Porchd\PorchdRepositoryContract;

/**
 * Class PorchdController.
 */
class PorchdController extends Controller
{
    /**
     * @var PorchdContract
     */
    protected $repository;

    /**
     * @param PorchdContract $porchd
     */
    public function __construct(PorchdRepositoryContract $porchd)
    {
          $this->repository = $porchd;
    }



    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable())
        ->escapeColumns(['name', 'description', 'long_description'])
        ->addColumn('actions', function ($repository) {
            return $repository->action_buttons;
        })
            ->withTrashed()
            ->make(true);
    }

    /**
     * @param ManagePorchdRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageRequest $request)
    {
      
        return view('backend.porchd.index');
    }
    /**
     * @param ManagePorchdRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getstates(ManageRequest $request)
    {
        $states =   $this->repository->getState();
        return response()->json($states);
    }


    /**
     * Create
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function create(ManageRequest $request)
    {
        return view('backend.porchd.create');
    }

     /**
     * Store
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        try {
            parse_str($request->get('data'), $data);
            $this->repository->create(
            [
                'data' => [
                    $data
                ]
            ]);
        } catch (Exception $e) {
        }


        //return redirect()->route('admin.advertiser.index')->withFlashSuccess(trans('alerts.backend.advertiser.created'));
    }

        /**
     * @param Porchd              $porchd
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function edit(Porchd $porchd, ManageRequest $request)
    {
        return view('backend.porchd.edit')->withPorchd($porchd);
    }

    
    /**
     * @param Porchd              $porchd
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function getInfo(ManageRequest $request)
    {
        $data = $this->repository->find($request->get('data'));
        return response()->json($data);
    }

    public function update(Porchd $porchd, ManageRequest $request)
    {
        parse_str($request->get('data'), $data);
        $this->repository->update($porchd, $data);
    }

    
    public function addPorchdImage($porchd, ManageRequest $request)
    {
        $image = $request->file('file');
        $response = $this->repository->addPorhdImage($image, $porchd);
 
        return response()->json($response);
    }

    public function imageList(ManageRequest $request)
    {
        $input  =  $request->get('data');
        $imagelist = $this->repository->find($input)->porchdImages()->get();
       return response()->json($imagelist);
    }
    

    public function saveImageInfo(ManageRequest $request)
    {
        $input  =  $request->get('data');
        $response = $this->repository->saveImageInfo($input);
       return response()->json($response);
    }

    public function claimPorchd(ManageRequest $request)
    {
        $input  =  $request->get('data');
        $response = $this->repository->claimPorchd($input);
       return response()->json($response);
    }
    
}
