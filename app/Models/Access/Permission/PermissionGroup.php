<?php

namespace App\Models\Access\Permission;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    //
    protected $table='permission_group';
}
