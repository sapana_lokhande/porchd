<?php namespace App\Http\Controllers\Backend\Advertiser;

/**
 * Class AdvertiserController
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */

use Illuminate\Http\Request;
use App\Models\Advertiser\Advertiser;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Advertiser\StoreRequest;
use App\Http\Requests\Backend\Advertiser\ManageRequest;
use App\Http\Requests\Backend\Advertiser\UpdateRequest;
use App\Repositories\Backend\Advertiser\AdvertiserRepositoryContract;

class AdvertiserController extends Controller
{
    /**
     * AdvertiserRepositoryContract
     *
     * @var object
     */
    protected $repository;

    /**
     * Construct
     *
     * @param AdvertiserRepositoryContract
     */
    public function __construct(AdvertiserRepositoryContract $repository)
    {
        $this->repository = new $repository;
    }

    /**
     * Invoke
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageRequest $request)
    {
        return Datatables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->escapeColumns(['name', 'url'])
            ->addColumn('actions', function ($advertiser) 
            {
                return $advertiser->action_buttons;
            })
            ->withTrashed()
            ->make(true);
    }

    /**
     * Index 
     *
     * @param ManageRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageRequest $request)
    {
        return view('backend.advertiser.index');
    }

    /**
     * Create 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function create(ManageRequest $request)
    {
        return view('backend.advertiser.create');
    }

    /**
     * Store 
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        $this->repository->create(
        [
            'data' => $request->only(
                'name',
                'url',
                'active'
            )
        ]);

        return redirect()->route('admin.advertiser.index')->withFlashSuccess(trans('alerts.backend.advertiser.created'));
    }

    /**
     * Show 
     *
     * @param Advertiser    $advertiser
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function show(Advertiser $advertiser, ManageRequest $request)
    {
        return view('backend.advertiser.show')
            ->withAdvertiser($advertiser);
    }

    /**
     * Edit 
     *
     * @param Advertiser    $advertiser
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function edit(Advertiser $advertiser, ManageRequest $request)
    {
        return view('backend.advertiser.edit')
            ->withAdvertiser($advertiser);
    }

    /**
     * Update 
     *
     * @param Advertiser    $advertiser
     * @param UpdateRequest $request
     *
     * @return mixed
     */
    public function update(Advertiser $advertiser, UpdateRequest $request)
    {
        $this->repository->update($advertiser,
            [
                'data' => $request->only(
                    'name',
                    'url',
                    'status'
                )
            ]);

        return redirect()->route('admin.advertiser.index')->withFlashSuccess(trans('alerts.backend.advertiser.updated'));
    }

    /**
     * Destory 
     *
     * @param Advertiser    $advertiser
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function destroy(Advertiser $advertiser, ManageRequest $request)
    {
        $this->repository->delete($advertiser);

        return redirect()->route('admin.advertiser.deleted')->withFlashSuccess(trans('alerts.backend.advertiser.deleted'));
    }

    /**
     * Get Deactivated 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function getDeactivated(ManageRequest $request)
    {
        return view('backend.advertiser.deactivated');
    }

    /**
     * Get Deleted 
     *
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function getDeleted(ManageRequest $request)
    {
        return view('backend.advertiser.deleted');
    }

    /**
     * Mark
     *
     * @param $id
     * @param $status
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function mark($id, $status, ManageRequest $request)
    {
        $advertiser = $this->repository->findOrThrowException($id);

        $this->repository->mark($advertiser, $status);

        return redirect()->route($status == 1 ? 'admin.advertiser.index' : 'admin.advertiser.deactivated')->withFlashSuccess(trans('alerts.backend.advertiser.updated'));
    }

    /**
     * Delete
     *
     * @param $id
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function delete($id, ManageRequest $request)
    {
        $advertiser = $this->repository->findOrThrowException($id);

        $this->repository->forceDelete($advertiser);

        return redirect()->route('admin.advertiser.deleted')->withFlashSuccess(trans('alerts.backend.advertiser.deleted_permanently'));
    }

    /**
     * Resote
     *
     * @param $id
     * @param ManageRequest $request
     *
     * @return mixed
     */
    public function restore($id, ManageRequest $request)
    {
        $advertiser = $this->repository->findOrThrowException($id);

        $this->repository->restore($advertiser);

        return redirect()->route('admin.advertiser.index')->withFlashSuccess(trans('alerts.backend.advertiser.restored'));
    }
}
