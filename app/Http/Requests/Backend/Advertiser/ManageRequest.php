<?php namespace App\Http\Requests\Backend\Advertiser;

/**
 * Class ManageRequest
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */

use App\Http\Requests\Request;

class ManageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
