<?php

namespace App\Repositories\Frontend\Access\User;

use App\Models\Access\User\User;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Models\Access\User\SocialLogin;
use App\Events\Frontend\Auth\UserConfirmed;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Helpers\SiteHelper;


/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param RoleRepository $role
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
        $this->sitehelper = new SiteHelper();
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    public function findByEmail($email)
    {
        return $this->query()->where('email', $email)->first();
    }

    /**
     * @param $token
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function findByConfirmationToken($token)
    {
        return $this->query()->where('confirmation_code', $token)->first();
    }

    /**
     * @param $token
     *
     * @return mixed
     */
    public function findByPasswordResetToken($token)
    {
        foreach (DB::table(config('auth.passwords.users.table'))->get() as $row) {
            if (password_verify($token, $row->token)) {
                return $this->findByEmail($row->email);
            }
        }

        return false;
    }

    /**
     * @param $token
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function getEmailForPasswordToken($token)
    {
        $rows = DB::table(config('auth.passwords.users.table'))->get();

        foreach ($rows as $row) {
            if (password_verify($token, $row->token)) {
                return $row->email;
            }
        }

        throw new GeneralException(trans('auth.unknown'));
    }

    /**
     * @param array $data
     * @param bool  $provider
     *
     * @return static
     */
    public function create(array $data, $provider = false)
    {

       
        $user = self::MODEL;
        $user = new $user;
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->status = 1;
        $user->password = $provider ? null : bcrypt($data['password']);

        $user->license_number = $data['license_number'];
        $user->state_assignment = $data['state_assignment'];
        $assignees_roles = $data['assignees_roles'];

        $user->phone_number = $data['phone_number'];
        $user->address1 = $data['address1'];
        $user->address2 = $data['address2'];
        $user->country = config('access.users.default_country');
        $user->state = $data['state'];
        $user->city = $data['city'];
        $user->zip = $data['zip'];

        $latLong   = $this->getLatLong($data['address1'] . ',' . $data['city'] . ',' . $data['state'] . ',' . $data['zip']);
        if (isset($latLong) && $latLong != '') 
        {
        
           $user->latitude   = $latLong['lat'];
           $user->longitude  = $latLong['lng'];
        
        }
        else 
        {
        
           $user->latitude   = "";
           $user->longitude  = "";
        
        }


        $confirm = false; // Whether or not they get an e-mail to confirm their account

        // If users require approval, confirmed is false regardless of account type
        if (config('access.users.requires_approval')) {
            $user->confirmed = 0; // No confirm e-mail sent, that defeats the purpose of manual approval
        } elseif (config('access.users.confirm_email')) { // If user must confirm email
            // If user is from social, already confirmed
            if ($provider) {
                $user->confirmed = 1; // E-mails are validated through the social platform
            } else {
                // Otherwise needs confirmation
                $user->confirmed = 0;
                $confirm = true;
            }
        } else {
            // Otherwise both are off and confirmed is default
            $user->confirmed = 1;
        }
        

        DB::transaction(function () use ($user,$assignees_roles) {

            if ($user->save()) {
                /*
                 * Add the default site role to the new user
                 */
      
                if(isset($assignees_roles)  && !empty($assignees_roles))
                {
                    $user->attachRole($this->role->getDefaultAgentRole());
                } 
                else 
                { 

                    $user->attachRole($this->role->getDefaultUserRole());
                }
                
            }
        });

        /*
         * If users have to confirm their email and this is not a social account,
         * and the account does not require admin approval
         * send the confirmation email
         *
         * If this is a social account they are confirmed through the social provider by default
         */
        if ($confirm) {
            // Pretty much only if account approval is off, confirm email is on, and this isn't a social account.
           // $user->notify(new UserNeedsConfirmation($user->confirmation_code));
        }

        /*
         * Return the user object
         */
        return $user;
    }

    /**
     * @param $data
     * @param $provider
     *
     * @return UserRepository|bool
     * @throws GeneralException
     */
    public function findOrCreateSocial($data, $provider)
    {
        // User email may not provided.
        $user_email = $data->email ?: "{$data->id}@{$provider}.com";

        // Check to see if there is a user with this email first.
        $user = $this->findByEmail($user_email);

        /*
         * If the user does not exist create them
         * The true flag indicate that it is a social account
         * Which triggers the script to use some default values in the create method
         */
        if (! $user) {
            // Registration is not enabled
            if (! config('access.users.registration')) {
                throw new GeneralException(trans('exceptions.frontend.auth.registration_disabled'));
            }

            // Get users first name and last name from their full name
            $nameParts = $this->getNameParts($data->getName());

            $user = $this->create([
                'first_name'  => $nameParts['first_name'],
                'last_name'  => $nameParts['last_name'],
                'email' => $user_email,
            ], true);
        }

        // See if the user has logged in with this social account before
        if (! $user->hasProvider($provider)) {
            // Gather the provider data for saving and associate it with the user
            $user->providers()->save(new SocialLogin([
                'provider'    => $provider,
                'provider_id' => $data->id,
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]));
        } else {
            // Update the users information, token and avatar can be updated.
            $user->providers()->update([
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]);
        }

        // Return the user object
        return $user;
    }

    /**
     * @param $token
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function confirmAccount($token)
    {
        $user = $this->findByConfirmationToken($token);

        if ($user->confirmed == 1) {
            throw new GeneralException(trans('exceptions.frontend.auth.confirmation.already_confirmed'));
        }

        if ($user->confirmation_code == $token) {
            $user->confirmed = 1;

            event(new UserConfirmed($user));

            return $user->save();
        }

        throw new GeneralException(trans('exceptions.frontend.auth.confirmation.mismatch'));
    }

    /**
     * @param $id
     * @param $input
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function updateProfile($id, $input)
    {
        
        $user = $this->find($id);
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->phone_number = $input['phone_number'];
        $user->address1 = $input['address1'];
        $user->country = config('access.users.default_country');
        $user->state = 2;
        $user->city = $input['city'];
        $user->zip = $input['zip'];
        $user->brokerage_name = $input['brokerage_name'];
        $user->state_assignment = $input['state_assignment'];
        $user->license_number = $input['license_number'];

        $file = $input['avatar'];
        if(isset($file))
        {
            $profile_image = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
            $source_image_path=public_path('avatar');
            $thumb_image_path1=public_path('avatar');
            $thumb_image_path2=public_path('avatar/thumbs');
            $input['avatar']->move($source_image_path, $profile_image);
            $this->sitehelper->generate_image_thumbnail($source_image_path.'/'.$profile_image,$thumb_image_path1.'/'.$profile_image,150,150);
            $this->sitehelper->generate_image_thumbnail($source_image_path.'/'.$profile_image,$thumb_image_path2.'/'.$profile_image,30,30);
            $user->avatar = $profile_image;
        }
        $stateName = $input['state'];
        $cityName  = $input['city'];
        $latLong   = $this->getLatLong($input['address1'] . ',' . $cityName . ',' . $stateName . ',' . $input['zip']);
        
        
        if (isset($latLong) && $latLong != '') 
        {
        
           $user->latitude   = $latLong['lat'];
           $user->longitude  = $latLong['lng'];
        
        }
        else 
        {
        
           $user->latitude   = "";
           $user->longitude  = "";
        
        }


        if ($user->canChangeEmail()) {
            //Address is not current address
            if ($user->email != $input['email']) {
                //Emails have to be unique
                if ($this->findByEmail($input['email'])) {
                    throw new GeneralException(trans('exceptions.frontend.auth.email_taken'));
                }

                // Force the user to re-verify his email address
                $user->confirmation_code = md5(uniqid(mt_rand(), true));
                $user->confirmed = 0;
                $user->email = $input['email'];
                $updated = $user->save();

                // Send the new confirmation e-mail
                $user->notify(new UserNeedsConfirmation($user->confirmation_code));

                return [
                    'success' => $updated,
                    'email_changed' => true,
                ];
            }
        }

        return $user->save();
    }

    /**
     * @param $input
     *
     * @throws GeneralException
     *
     * @return mixed
     */
    public function changePassword($input)
    {
        $user = $this->find(access()->id());

        if (Hash::check($input['old_password'], $user->password)) {
            $user->password = bcrypt($input['password']);

            return $user->save();
        }

        throw new GeneralException(trans('exceptions.frontend.auth.password.change_mismatch'));
    }

    /**
     * @param $fullName
     *
     * @return array
     */
    protected function getNameParts($fullName)
    {
        $parts = array_values(array_filter(explode(' ', $fullName)));

        $size = count($parts);

        $result = [];

        if (empty($parts)) {
            $result['first_name'] = null;
            $result['last_name'] = null;
        }

        if (! empty($parts) && $size == 1) {
            $result['first_name'] = $parts[0];
            $result['last_name'] = null;
        }

        if (! empty($parts) && $size >= 2) {
            $result['first_name'] = $parts[0];
            $result['last_name'] = $parts[1];
        }

        return $result;
    }


    //get lat, log from address
    public function getLatLong($address) {

        $googleQuery = $address;
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($googleQuery) . '&sensor=false';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch), true);
        if (isset($response) && $response['status'] != 'ZERO_RESULTS') {
            if (!empty($response['results'][0]['geometry']['location'])) {
                $latLong = $response['results'][0]['geometry']['location'];
            } else {
                $latLong = '';
            }
        } else {
            $latLong = '';
        }
        return $latLong;
    }



}
