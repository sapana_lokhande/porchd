@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.permissions.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>{{ trans('labels.backend.access.permissions.management') }}</h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.permissions.management') }}</h3>

            <div class="box-tools pull-right">
              <div class="pull-right mb-10 hidden-sm hidden-xs">
                  {{ link_to_route('admin.access.permission.index', trans('menus.backend.access.permissions.all'), [], ['class' => 'btn btn-primary btn-xs']) }}
                  {{ link_to_route('admin.access.permission.create', trans('menus.backend.access.permissions.create'), [], ['class' => 'btn btn-success btn-xs']) }}
              </div><!--pull right-->

              <div class="pull-right mb-10 hidden-lg hidden-md">
                  <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          {{ trans('menus.backend.access.permissions.main') }} <span class="caret"></span>
                      </button>

                      <ul class="dropdown-menu" role="menu">
                          <li>{{ link_to_route('admin.access.permission.index', trans('menus.backend.access.permissions.all')) }}</li>
                          <li>{{ link_to_route('admin.access.permission.create', trans('menus.backend.access.permissions.create')) }}</li>
                      </ul>
                  </div><!--btn group-->
              </div><!--pull right-->

              <div class="clearfix"></div>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="permission-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.access.permissions.table.disp_name') }}</th>
                            <th>{{ trans('labels.backend.access.permissions.table.name') }}</th>
                            <th>{{ trans('labels.backend.access.permissions.table.sort') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('Permission') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}

    <script>
        $(function() {
            $('#permission-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.access.permission.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'display_name', name: 'display_name'},
                    {data: 'name', name: 'name'},
                    {data: 'sort', name: 'sort'},
                    {data: 'actions', name: 'actions'}
                ],
                 order: [[2, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection
