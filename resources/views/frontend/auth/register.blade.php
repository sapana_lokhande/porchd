@extends('frontend.layouts.app')

@section('title', app_name() . ' | Register')

@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('labels.frontend.auth.register_box_title') }}</div>

                <div class="panel-body">

                    {{ Form::open(['route' => 'frontend.auth.register.post', 'class' => 'form-horizontal']) }}

                    <div class="form-group">
                        {{ Form::label('first_name', trans('validation.attributes.frontend.first_name'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('first_name', null,
                            ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('last_name', trans('validation.attributes.frontend.last_name'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('last_name', null,
                            ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('phone_number', trans('validation.attributes.frontend.phone_number'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('phone_number', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.phone_number')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('address1', trans('validation.attributes.frontend.address1'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('address1', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.address1')]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('address2', trans('validation.attributes.frontend.address2'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('address2', null, ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.address2')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('state', trans('validation.attributes.frontend.state'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::select('state', $state, 'all', ['class' => 'form-control']) }}
                           
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('city', trans('validation.attributes.frontend.city'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('city', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.city')]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('zip', trans('validation.attributes.frontend.zip'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('zip', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.zip')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                            {{ Form::label('birthdate', trans('validation.attributes.frontend.birthdate'),
                            ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                              {{ Form::text('birthdate', null,
                                ['class' => 'form-control date', 'maxlength' => '191','placeholder' => trans('validation.attributes.frontend.birthdate')]) }}
                            </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('role', trans('validation.attributes.frontend.role'),
                        ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            <input type="checkbox" value="1" class ="assignees_roles" name="assignees_roles"  /> <label for="Real Estate Agent">Yes</label>
                            <input type="checkbox" value="2" class ="assignees_roles" name="assignees_roles_no"  /> <label for="Real Estate Agent">No</label>
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div id="real_estate_agent_div">
                        <div class="form-group" >
                                {{ Form::label('license_number', trans('validation.attributes.frontend.license_number'),
                                ['class' => 'col-md-4 control-label']) }}
                                <div class="col-md-6">
                                  {{ Form::text('license_number', null,
                                    ['class' => 'form-control', 'maxlength' => '191',  'placeholder' => trans('validation.attributes.frontend.license_number')]) }}
                                </div><!--col-md-6-->
                        </div><!--form-group-->
                        <div class="form-group">
                                {{ Form::label('state_assignment', trans('validation.attributes.frontend.state_assignment'),
                                ['class' => 'col-md-4 control-label']) }}
                                <div class="col-md-6">
                                  {{ Form::text('state_assignment', null,
                                    ['class' => 'form-control', 'maxlength' => '191', 'placeholder' => trans('validation.attributes.frontend.state_assignment')]) }}
                                </div><!--col-md-6-->
                        </div><!--form-group-->
                    </div>
                   
                    @if (config('access.captcha.registration'))
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {!! Form::captcha() !!}
                                {{ Form::hidden('captcha_status', 'true') }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->
                    @endif

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit(trans('labels.frontend.auth.register_button'), ['class' => 'btn btn-primary']) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    {{ Form::close() }}

                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
@endsection

@section('after-scripts')
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>
 <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    {{ Html::script('js/frontend/register/script.js') }}
    @if (config('access.captcha.registration'))
        {!! Captcha::script() !!}
    @endif
@endsection