<?php namespace App\Models\UserPorchdAccount;

/**
 * Class UserPorchdAccount
 *
 * @author Sapana Lokhande sapana.lokhande@satincorp.com
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UserPorchdAccount\Traits\Relationship\UserPorchdAccountRelationship;

class UserPorchdAccount extends Model
{

    use SoftDeletes,
        UserPorchdAccountRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'user_porchd_account';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id','allowed_porch','allowed_images','allowed_claim'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = [];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {   
        parent::__construct($attributes);
     
        $this->table = config('backend.user_porchd_account_table');
    }

}
