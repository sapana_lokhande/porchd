<?php namespace App\Events\Backend\Advertiser;

use Illuminate\Queue\SerializesModels;

/**
 * Class AdvertiserReactivated.
 *
 * @author Sachin Kumar sachin.kumar@satincorp.com
 */
class AdvertiserReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $advertiser;

    /**
     * @param $advertiser
     */
    public function __construct($advertiser)
    {
        $this->advertiser = $advertiser;
    }
}
